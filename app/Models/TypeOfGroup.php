<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfGroup extends Model
{
    use HasFactory;

    protected $table = "type_of_group";

    protected $fillable = ["type_of_group", "class_of_type", "title_of_type", "category"];
}
