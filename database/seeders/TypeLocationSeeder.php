<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        DB::table('type_location')->insert([
            'type_location' => 'Город',
            'type_location_code' => 'city',
        ]);
        DB::table('type_location')->insert([
            'type_location' => 'Село',
            'type_location_code' => 'village',
        ]);
    }
}
