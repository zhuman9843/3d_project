<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSputnikBoutiques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sputnik_boutiques', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->integer('teach_area')->nullable();
            $table->string('name');
            $table->string('floor');
            $table->string('ip_name');
            $table->string('brand');
            $table->float('area')->round(2);
            $table->float('base_rate')->round(2);
            $table->float('exploitation_rate')->round(2);
            $table->float('rate')->round(2);
            $table->float('rent')->round(2);
            $table->float('total')->round(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sputnik_boutiques');
    }
}
