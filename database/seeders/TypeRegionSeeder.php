<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_region')->insert([
            'type_region' => 'Обычный регион',
            'code_of_region' => 'normal',
        ]);
        DB::table('type_region')->insert([
            'type_region' => 'В зоне радиации',
            'code_of_region' => 'rad',
        ]);
        DB::table('type_region')->insert([
            'type_region' => 'В зоне экологии',
            'code_of_region' => 'eco',
        ]);
    }
}
