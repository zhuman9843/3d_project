(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_dashboard_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./resources/js/views/dashboard/child_to_day.js?vue&type=script&lang=js&external":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./resources/js/views/dashboard/child_to_day.js?vue&type=script&lang=js&external ***!
  \********************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _chart_options_child_to_day_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chart_options_child_to_day.json */ "./resources/js/views/dashboard/chart_options_child_to_day.json");
/* harmony import */ var _chart_options_attendance_child_to_day_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./chart_options_attendance_child_to_day.json */ "./resources/js/views/dashboard/chart_options_attendance_child_to_day.json");
/* harmony import */ var _chart_options_budget_child_to_day_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chart_options_budget_child_to_day.json */ "./resources/js/views/dashboard/chart_options_budget_child_to_day.json");
/* harmony import */ var _chart_options_budget_difference_child_to_day_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chart_options_budget_difference_child_to_day.json */ "./resources/js/views/dashboard/chart_options_budget_difference_child_to_day.json");
/* harmony import */ var _chart_options_reason_of_missing_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chart_options_reason_of_missing.json */ "./resources/js/views/dashboard/chart_options_reason_of_missing.json");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/components/modal/modal.js");
/* harmony import */ var _components_Nav__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../components/Nav */ "./resources/js/components/Nav.vue");
/* harmony import */ var xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! xlsx-js-style */ "./node_modules/xlsx-js-style/dist/xlsx.min.js");
/* harmony import */ var xlsx_js_style__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }





// import VueApexCharts from 'vue-apexcharts';





/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'ChildToDay',
  components: {
    Nav: _components_Nav__WEBPACK_IMPORTED_MODULE_5__["default"],
    XLSX: xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__,
    BootstrapVue: bootstrap_vue__WEBPACK_IMPORTED_MODULE_7__.BootstrapVue,
    BModal: bootstrap_vue__WEBPACK_IMPORTED_MODULE_8__.BModal
    // apexchart: VueApexCharts
  },
  data: function data() {
    return {
      chartOptionsChildToDay: _chart_options_child_to_day_json__WEBPACK_IMPORTED_MODULE_0__,
      chartOptionsAttendanceChildToDay: _chart_options_attendance_child_to_day_json__WEBPACK_IMPORTED_MODULE_1__,
      chartOptionsBudgetChildToDay: _chart_options_budget_child_to_day_json__WEBPACK_IMPORTED_MODULE_2__,
      chartOptionsBudgetDifferenceChildToDay: _chart_options_budget_difference_child_to_day_json__WEBPACK_IMPORTED_MODULE_3__,
      chartsOptionsReasonOfMissing: _chart_options_reason_of_missing_json__WEBPACK_IMPORTED_MODULE_4__,
      fullscreenTimer: null,
      currentDayAttendance: '',
      isDragging: false,
      buttonText: 'Перетащите вверх',
      screenWidth: window.innerWidth,
      // currentRegion: '',
      mainData: '',
      regions: [],
      groups: '',
      counter: 0,
      fetchedHeaders: '',
      fetchedRows: '',
      fetchedKeys: '',
      searchKindergarten: '',
      lastDownloadedDate: '',
      searchChild: '',
      kindergartenDate: '2023-10',
      selectedKindergarten: [],
      selectedGroup: 0,
      selectedTitle: 'Все области',
      clickedKindergarten: '',
      budgetData: '',
      clickedGroup: '',
      selectedRegion: 0,
      allKindergartenInClicked: [],
      amountOfTypeAttendance: [],
      typeOfAttendance: [],
      isKindergartenAttendance: false,
      isShowTable: false,
      isShowTarify: false,
      isFilter: false,
      isChartsChildToDay: false,
      isChartsAttendanceChildToDay: false,
      isChartsBudgetChildToDay: false,
      isChartsBudgetDifferenceChildToDay: false,
      selectedRowIndex: null,
      isExpanded: true,
      currentPage: 'amount',
      currentComponent: 'graph',
      itemId: null,
      childToDayFiltered: '',
      chartDataFiltered: '',
      chartAttendanceDataFiltered: '',
      listOfData: '',
      options: {},
      isDisable: true,
      startDate: '2023-01',
      endDate: '2023-12',
      listOfMonths: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      listOfMonthKeys: {
        '0': 0,
        '1': 0,
        '2': 0,
        '3': 0,
        '4': 0,
        '5': 0,
        '6': 0,
        '7': 0,
        '8': 0,
        '9': 0,
        '10': 0,
        '11': 0
      },
      filteredMonthKeys: {}
    };
  },
  watch: {
    'startDate': function startDate() {
      this.fetchOptions();
    },
    'endDate': function endDate() {
      this.fetchOptions();
    },
    'selectedRegion': function selectedRegion() {
      // if (this.selectedRegion == 0) {
      //   location.reload();
      // }
      this.fetchOptions();
    },
    'searchKindergarten': function searchKindergarten() {
      this.fetchOptions();
    },
    'kindergartenMonth': function kindergartenMonth() {
      this.showTable(this.clickedKindergarten);
    },
    'searchChild': function searchChild() {
      this.showTable(this.clickedKindergarten);
    },
    'selectedGroup': function selectedGroup() {
      this.showTable(this.clickedKindergarten);
    }
  },
  computed: _objectSpread(_objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_9__.mapState)({
    isLoggedIn: "isLoggedIn"
  })), (0,vuex__WEBPACK_IMPORTED_MODULE_9__.mapGetters)(["user"])), {}, {
    governmentId: function governmentId() {
      return this.user.government_id;
    },
    isLargeScreen: function isLargeScreen() {
      if (this.screenWidth > 1024) {
        return true;
      } else {
        return false;
      }
    },
    isMiddleScreen: function isMiddleScreen() {
      if (this.screenWidth < 1024) {
        return true;
      } else {
        return false;
      }
    },
    days: function days() {
      var days = [];
      Object.values(this.fetchedHeaders).forEach(function (item) {
        if (typeof item == 'number') {
          days.push(item);
        }
      });
      return days;
    },
    arrayTreeObj: function arrayTreeObj() {
      var vm = this;
      var newObj = [];
      vm.putInsideParentElement(vm.childToDay, newObj, 0, vm.itemId, true);
      return newObj;
    },
    chartsAttendanceChildToDay: function chartsAttendanceChildToDay() {
      var _this = this;
      this.chartOptionsAttendanceChildToDay.chartOptions = {
        chart: {
          stacked: true
        },
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: function formatter(val) {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "количество",
            style: {
              fontSize: "12px",
              fontWeight: 600,
              fontFamily: "Nunito",
              color: "#263238"
            }
          },
          labels: {
            formatter: function formatter(y) {
              if (y) return y.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        }
      };
      var series = [];
      var sickMonthKeys = {};
      var weekendMonthKeys = {};
      var noReasonMonthKeys = {};
      Object.keys(this.filteredMonthKeys).forEach(function (key) {
        _this.$set(sickMonthKeys, key, '0');
        _this.$set(weekendMonthKeys, key, '0');
        _this.$set(noReasonMonthKeys, key, '0');
      });
      var sickData = {
        'name': 'Болеют',
        'data': sickMonthKeys
      };
      var weekendData = {
        'name': 'Отпуск',
        'data': weekendMonthKeys
      };
      var noReasonData = {
        'name': 'Пропуск',
        'data': noReasonMonthKeys
      };
      for (var prop in this.chartAttendanceData) {
        var data = this.chartAttendanceData[prop].data;
        Object.entries(data).forEach(function (entry) {
          var _entry = _slicedToArray(entry, 2),
            key = _entry[0],
            value = _entry[1];
          var month = key - 1;
          sickData.data[month] = value.sick;
          weekendData.data[month] = value.weekend;
          noReasonData.data[month] = value.missing;
        });
      }
      sickData.data = Object.values(sickData.data);
      weekendData.data = Object.values(weekendData.data);
      noReasonData.data = Object.values(noReasonData.data);
      series.push(sickData);
      series.push(weekendData);
      series.push(noReasonData);
      this.chartOptionsAttendanceChildToDay.series = series;
      this.chartOptionsAttendanceChildToDay.chartOptions.colors = ['#EDA774', '#4F81BD', '#B1B1B7'];
      return this.chartOptionsAttendanceChildToDay;
    },
    chartsChildToDay: function chartsChildToDay() {
      var _this2 = this;
      this.chartOptionsChildToDay.chartOptions = {
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: function formatter(val) {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "количество",
            style: {
              fontSize: "12px",
              fontWeight: 600,
              fontFamily: "Nunito",
              color: "#263238"
            }
          },
          labels: {
            formatter: function formatter(y) {
              if (y) return y.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        }
      };
      var series = [];
      var planMonthKeys = {};
      var factMonthKeys = {};
      Object.keys(this.filteredMonthKeys).forEach(function (key) {
        _this2.$set(planMonthKeys, key, '0');
        _this2.$set(factMonthKeys, key, '0');
      });
      var monthPlanData = {
        'name': 'План',
        'data': planMonthKeys
      };
      var monthFactData = {
        'name': 'Факт',
        'data': factMonthKeys
      };
      for (var prop in this.chartData) {
        var data = this.chartData[prop].data;
        Object.entries(data).forEach(function (entry) {
          var _entry2 = _slicedToArray(entry, 2),
            key = _entry2[0],
            value = _entry2[1];
          var month = key - 1;
          monthPlanData.data[month] = value.plan_child_to_day;
          monthFactData.data[month] = value.fact_child_to_day;
        });
      }
      monthPlanData.data = Object.values(monthPlanData.data);
      monthFactData.data = Object.values(monthFactData.data);
      series.push(monthPlanData);
      series.push(monthFactData);
      this.chartOptionsChildToDay.series = series;
      this.chartOptionsChildToDay.chartOptions.colors = ['#27AE60', '#D7C41E'];
      return this.chartOptionsChildToDay;
    },
    chartsBudgetChildToDay: function chartsBudgetChildToDay() {
      var _this3 = this;
      this.chartOptionsBudgetChildToDay.chartOptions = {
        chart: {
          stacked: false
        },
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: function formatter(val) {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "тыс. тенге",
            style: {
              fontSize: "12px",
              fontWeight: 600,
              fontFamily: "Nunito",
              color: "#263238"
            }
          },
          labels: {
            formatter: function formatter(y) {
              if (y) return y.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        }
      };
      var series = [];
      var planMonthKeys = {};
      var factMonthKeys = {};
      Object.keys(this.filteredMonthKeys).forEach(function (key) {
        _this3.$set(planMonthKeys, key, '0');
        _this3.$set(factMonthKeys, key, '0');
      });
      var monthPlanData = {
        'name': 'План',
        'data': planMonthKeys
      };
      var monthFactData = {
        'name': 'Факт',
        'data': factMonthKeys
      };
      for (var prop in this.chartData) {
        var data = this.chartData[prop].data;
        Object.entries(data).forEach(function (entry) {
          var _entry3 = _slicedToArray(entry, 2),
            key = _entry3[0],
            value = _entry3[1];
          var month = key - 1;
          monthPlanData.data[month] = value.budget_plan / 1000;
          monthFactData.data[month] = value.budget_fact / 1000;
        });
      }
      monthPlanData.data = Object.values(monthPlanData.data);
      monthFactData.data = Object.values(monthFactData.data);
      series.push(monthPlanData);
      series.push(monthFactData);
      this.chartOptionsBudgetChildToDay.series = series;
      this.chartOptionsBudgetChildToDay.chartOptions.colors = ['#27AE60', '#D7C41E'];
      return this.chartOptionsBudgetChildToDay;
    },
    chartsBudgetDifferenceChildToDay: function chartsBudgetDifferenceChildToDay() {
      var _this4 = this;
      this.chartOptionsBudgetDifferenceChildToDay.chartOptions = {
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: function formatter(val) {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "тыс. тенге",
            style: {
              fontSize: "12px",
              fontWeight: 600,
              fontFamily: "Nunito",
              color: "#263238"
            }
          },
          labels: {
            formatter: function formatter(y) {
              if (y) return y.toLocaleString(undefined, {
                maximumFractionDigits: 1
              });
            }
          }
        }
      };
      var series = [];
      var diffMonthKeys = {};
      Object.keys(this.filteredMonthKeys).forEach(function (key) {
        _this4.$set(diffMonthKeys, key, '0');
      });
      var differenceData = {
        'name': 'План - Факт',
        'data': diffMonthKeys
      };
      for (var prop in this.chartData) {
        var data = this.chartData[prop].data;
        Object.entries(data).forEach(function (entry) {
          var _entry4 = _slicedToArray(entry, 2),
            key = _entry4[0],
            value = _entry4[1];
          var month = key - 1;
          differenceData.data[month] = (value.budget_plan - value.budget_fact) / 1000;
        });
      }
      differenceData.data = Object.values(differenceData.data);
      series.push(differenceData);
      this.chartOptionsBudgetDifferenceChildToDay.series = series;
      this.chartOptionsBudgetDifferenceChildToDay.chartOptions.colors = ['#eda774'];
      return this.chartOptionsBudgetDifferenceChildToDay;
    },
    currentDate: function currentDate() {
      var currentDate = new Date();
      var year = currentDate.getFullYear();
      var month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
      var formattedDate = "".concat(year, "-").concat(month);
      return formattedDate;
    },
    kindergartenTableMonth: function kindergartenTableMonth() {
      var options = {
        month: 'long',
        year: 'numeric'
      };
      return new Date(this.kindergartenDate).toLocaleDateString('ru-RU', options);
    },
    currentMonth: function currentMonth() {
      var options = {
        month: 'long',
        year: 'numeric'
      };
      return new Date().toLocaleDateString('ru-RU', options);
    },
    months: function months() {
      var monthNames = [];
      var monthNumbers = [];
      var monthAttributes = [];
      var startDate = new Date(this.startDate).getMonth() + 1;
      var endDate = new Date(this.endDate).getMonth() + 1;
      for (var i = 0; i < new Date(this.endDate).getMonth() + 1; i++) {
        if (startDate <= i + 1 && i <= endDate) {
          monthNumbers.push((i + 1).toString().padStart(2, '0'));
          monthNumbers.push((i + 1).toString().padStart(2, '0'));
          monthNames.push(this.listOfMonths[i]);
          monthAttributes.push('План');
          monthAttributes.push('Факт');
        }
      }
      return {
        'numbers': monthNumbers,
        'names': monthNames,
        'attributes': monthAttributes
      };
    },
    lastMonth: function lastMonth() {
      return this.months['numbers'].slice(-1)[0];
      // return this.months['numbers'][0] 
    },
    childToDay: function childToDay() {
      return this.childToDayFiltered;
    },
    chartData: function chartData() {
      return this.chartDataFiltered;
    },
    chartAttendanceData: function chartAttendanceData() {
      return this.chartAttendanceDataFiltered;
    },
    kindergartenMonth: function kindergartenMonth() {
      var month = (new Date(this.kindergartenDate + '-01').getMonth() + 1).toString().padStart(2, '0');
      return month;
    },
    classOfAttendance: function classOfAttendance() {
      if (this.typeOfAttendance == 'missing') {
        return 'missing_day';
      } else if (this.typeOfAttendance == 'sick') {
        return 'sick_day';
      } else if (this.typeOfAttendance == 'weekend') {
        return 'wekeend_day';
      }
    },
    amountDays: function amountDays() {
      var month = new Date(this.kindergartenDate).getMonth() + 1;
      var countDays = new Date(2023, month, 0).getDate();
      return countDays;
    },
    blindData: function blindData() {
      Object.values(this.chartData);
    },
    currentDayAttend: function currentDayAttend() {
      return this.currentDayAttendance;
    },
    chartsReasonOfMissing: function chartsReasonOfMissing() {
      this.chartsOptionsReasonOfMissing.series = [this.currentDayAttend.missed, this.currentDayAttend.sick, this.currentDayAttend.weekend];
      this.chartsOptionsReasonOfMissing.chartOptions = {
        plotOptions: {
          pie: {
            donut: {
              size: '80%',
              labels: {
                show: true,
                total: {
                  show: true,
                  showAlways: true,
                  label: 'Всего:',
                  fontSize: 32,
                  color: '#666666',
                  fontWeight: '700'
                }
              }
            }
          }
        },
        chart: {
          type: 'donut'
        },
        dataLabels: {
          enabled: false
        },
        colors: ['#B1B1B7', '#EDA774', '#4F81BD'],
        labels: ["Пропуск", "Больничный", "Отпуск"],
        legend: {
          show: false
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: '100%'
            },
            legend: {
              show: false
            }
          }
        }]
      };
      return this.chartsOptionsReasonOfMissing;
    },
    exactlyCurrentDate: function exactlyCurrentDate() {
      var date = new Date();
      var day = String(date.getDate()).padStart(2, '0');
      var month = String(date.getMonth() + 1).padStart(2, '0');
      var year = String(date.getFullYear());
      return "".concat(day, ".").concat(month, ".").concat(year);
    }
  }),
  methods: {
    updatePage: function updatePage() {},
    updateScreenWidth: function updateScreenWidth() {
      this.screenWidth = window.innerWidth;
    },
    selectGroup: function selectGroup(event) {
      this.selectedGroup = event.target.value;
      if (this.selectedGroup == 0) {
        this.selectedGroupInfo.name = "Все группы";
        this.selectedGroupInfo.type_group = "Все возможные режимы";
      }
      this.selectedGroupInfo = Array.from(this.groups).filter(function (group) {
        return group.id == event.target.value;
      })[0];
    },
    getGroups: function getGroups(bin) {
      var _this5 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var res;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios.get('/api/get-groups-to-dashboard?bin=' + bin);
            case 2:
              res = _context.sent;
              _this5.groups = res.data;
            case 4:
            case "end":
              return _context.stop();
          }
        }, _callee);
      }))();
    },
    updateAttendance: function updateAttendance() {
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              location.reload();
            case 1:
            case "end":
              return _context2.stop();
          }
        }, _callee2);
      }))();
    },
    exportTableToExcel: function exportTableToExcel() {
      var wb = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.book_new();
      var ws = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.table_to_sheet(document.querySelector('.table_child'));
      document.querySelector('.hide_none').classList.add('show_table');
      ws['!cols'] = [{
        width: 5
      }, {
        width: 40
      }, {
        width: 15
      }, {
        width: 15
      }, {
        width: 15
      }, {
        width: 15
      }];
      ws['!margins'] = {
        top: 1.25,
        bottom: 1.25,
        header: 1.5,
        footer: 0.75
      };
      var border = {
        top: {
          style: 'thin',
          color: 'black'
        },
        bottom: {
          style: 'thin',
          color: 'black'
        },
        left: {
          style: 'thin',
          color: 'black'
        },
        right: {
          style: 'thin',
          color: 'black'
        }
      };
      var borderBottom = {
        bottom: {
          style: 'thin',
          color: 'black'
        }
      };
      var alignment = {
        vertical: 'center',
        horizontal: 'center',
        wrapText: true
      };
      Object.keys(ws).forEach(function (cell) {
        if (cell !== '!ref' && cell !== '!fullref' && cell !== '!merges' && cell !== '!margins') {
          var style = ws[cell].s || {};
          if (ws[cell].v == '..') {
            style.border = borderBottom;
          } else {
            style.border = border;
          }
          style.alignment = alignment;
          style.padding = {
            top: 5,
            bottom: 5,
            left: 5,
            right: 5
          };
          ws[cell].s = style;
          if (ws[cell].v == '...' || ws[cell].v == '..') {
            ws[cell].v = '';
          }
        }
      });
      ws['B2'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'center',
          wrapText: true
        },
        font: {
          bold: true,
          underline: true
        }
      };
      ws['B3'].s = {
        border: {},
        alignment: {
          vertical: 'top',
          horizontal: 'center',
          wrapText: true
        },
        font: {
          sz: 9,
          italic: true
        }
      };
      ws['B6'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'center',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.book_append_sheet(wb, ws, 'Табель');
      var excel = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.writeFile(wb, 'Табель посещаемости.xlsx');
      document.querySelector('.show_table').remove('show_table');
      return excel;
    },
    exportTarifyToExcel: function exportTarifyToExcel() {
      var wb = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.book_new();
      var ws = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.table_to_sheet(document.querySelector('.table_tarify'));
      ws['!cols'] = [{
        width: 20
      }, {
        width: 15
      }, {
        width: 10
      }, {
        width: 15
      }, {
        width: 15
      }, {
        width: 15
      }, {
        width: 15
      }];
      ws['!margins'] = {
        top: 1.25,
        bottom: 1.25,
        header: 1.5,
        footer: 0.75
      };
      var border = {
        top: {
          style: 'thin',
          color: 'black'
        },
        bottom: {
          style: 'thin',
          color: 'black'
        },
        left: {
          style: 'thin',
          color: 'black'
        },
        right: {
          style: 'thin',
          color: 'black'
        }
      };
      var borderBottom = {
        bottom: {
          style: 'thin',
          color: 'black'
        }
      };
      var alignment = {
        vertical: 'center',
        horizontal: 'center',
        wrapText: true
      };
      Object.keys(ws).forEach(function (cell) {
        if (cell !== '!ref' && cell !== '!fullref' && cell !== '!merges' && cell !== '!margins') {
          var style = ws[cell].s || {};
          if (ws[cell].v == '..') {
            style.border = borderBottom;
          } else {
            style.border = border;
          }
          if (ws[cell].v == '...') {
            ws[cell].v = '';
          }
          style.alignment = alignment;
          style.padding = {
            top: 5,
            bottom: 5,
            left: 5,
            right: 5
          };
          ws[cell].s = style;
        }
      });
      ws['B2'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      ws['B3'].s = {
        border: {},
        alignment: {
          vertical: 'top',
          horizontal: 'left',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      ws['B4'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      ws['B5'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      ws['B7'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'center',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      ws['B9'].s = {
        border: {},
        alignment: {
          vertical: 'center',
          horizontal: 'center',
          wrapText: true
        },
        font: {
          bold: true
        }
      };
      xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.book_append_sheet(wb, ws, 'Расчет бюджета');
      var excel = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.writeFile(wb, 'Таблица расчета бюджета.xlsx');
      return excel;
    },
    showAttendance: function showAttendance(type) {
      var _this6 = this;
      this.typeOfAttendance = type;
      this.allKindergartenInClicked = [];
      this.amountOfTypeAttendance = [];
      var startDate = new Date(this.startDate).getMonth() + 1;
      var endDate = new Date(this.endDate).getMonth() + 1;
      if (this.clickedGroup) {
        this.getKindergarten(this.clickedGroup);
      } else {
        Object.values(this.chartData).forEach(function (item) {
          _this6.getKindergarten(item);
        });
      }
      this.allKindergartenInClicked.forEach(function (kingarten) {
        var filteredByMonth = Object.keys(kingarten.data).filter(function (key) {
          if (startDate <= key && key <= endDate) return true;
        }).reduce(function (obj, key) {
          obj[key] = kingarten.data[key];
          return obj;
        }, {});
        kingarten.data = filteredByMonth;
      });
      Object.values(this.allKindergartenInClicked).forEach(function (kindergarten) {
        var sum = 0;
        for (var key in kindergarten.data) {
          if (kindergarten.data[key][type]) {
            sum += kindergarten.data[key][type];
          }
        }
        _this6.amountOfTypeAttendance.push({
          'name': kindergarten.name,
          'sum': sum
        });
      });
      this.isKindergartenAttendance = true;
    },
    closeAttendance: function closeAttendance() {
      this.isKindergartenAttendance = false;
    },
    getKindergarten: function getKindergarten(item) {
      var _this7 = this;
      if (Object.keys(item).includes('child')) {
        Object.values(item.child).forEach(function (insideItem) {
          _this7.getKindergarten(insideItem);
        });
      } else {
        this.allKindergartenInClicked.push(item);
      }
    },
    tableHeaderStyle: function tableHeaderStyle(header) {
      if (header == 'ФИО') {
        return "width: 15%;";
      } else if (typeof header == 'string') {
        return "width: 3%;";
      } else if (typeof header == 'number') {
        return "width: 2% !important;";
      }
    },
    headerDay: function headerDay() {
      return 'colspan:' + this.amountDays;
    },
    dayClass: function dayClass(dayType) {
      if (dayType.type_of_day == 2) {
        return "relax_day";
      }
    },
    attendanceClass: function attendanceClass(attendance) {
      if (attendance.attendance == 6 && attendance.type_of_day != 2) {
        return "missing_day";
      } else if (attendance.attendance == 4) {
        return "sick_day";
      } else if (attendance.attendance == 5) {
        return "wekeend_day";
      } else if (attendance.attendance == 14) {
        return "edited_sick_day";
      } else if (attendance.attendance == 15) {
        return "edited_wekeend_day";
      } else if (attendance.attendance == 16) {
        return "edited_missing_day";
      } else if (attendance.attendance == 12) {
        return "edited_attended_day";
      } else {
        return "attended_day";
      }
    },
    showTable: function showTable(item) {
      var _this8 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
        var tarify, table, filteredChild;
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              if (item.child) {
                _context3.next = 18;
                break;
              }
              if (!(_this8.currentPage === 'budget')) {
                _context3.next = 9;
                break;
              }
              _this8.isShowTarify = true;
              _context3.next = 5;
              return axios.get('api/get-tarify-data?bin=' + item.bin + '&month=' + _this8.kindergartenMonth);
            case 5:
              tarify = _context3.sent;
              _this8.budgetData = tarify.data;
              _context3.next = 18;
              break;
            case 9:
              if (!(_this8.currentPage === 'amount')) {
                _context3.next = 18;
                break;
              }
              _this8.isShowTable = true;
              _this8.getGroups(item.bin);
              _context3.next = 14;
              return axios.get('api/get-table-data?bin=' + item.bin + '&month=' + _this8.kindergartenMonth + '&group=' + _this8.selectedGroup);
            case 14:
              table = _context3.sent;
              _this8.fetchedHeaders = table.data.headers;
              _this8.fetchedKeys = table.data.keys;
              if (_this8.searchChild) {
                filteredChild = Object.entries(table.data.data).reduce(function (acc, _ref) {
                  var _ref2 = _slicedToArray(_ref, 2),
                    key = _ref2[0],
                    value = _ref2[1];
                  if (value.name.toLowerCase().includes(_this8.searchChild.toLowerCase())) {
                    acc[key] = value;
                  }
                  return acc;
                }, {});
                _this8.fetchedRows = filteredChild;
              } else {
                _this8.fetchedRows = table.data.data;
              }
            case 18:
            case "end":
              return _context3.stop();
          }
        }, _callee3);
      }))();
    },
    closeTarify: function closeTarify() {
      this.isShowTarify = false;
    },
    closeTable: function closeTable() {
      this.isShowTable = false;
    },
    onChange: function onChange(event) {
      this.selectedRegion = event.target.value;
    },
    getRegions: function getRegions() {
      var _this9 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
        var res;
        return _regeneratorRuntime().wrap(function _callee4$(_context4) {
          while (1) switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return axios.get('/get-regions');
            case 2:
              res = _context4.sent;
              if (_this9.user.government_id !== 0) {
                Object.values(res.data).forEach(function (region) {
                  if (region.id == _this9.user.government_id) {
                    _this9.regions.push(region);
                    _this9.selectedRegion = region.id;
                  }
                });
              } else {
                _this9.regions = res.data;
              }
            case 4:
            case "end":
              return _context4.stop();
          }
        }, _callee4);
      }))();
    },
    showModal: function showModal(chart) {
      if (chart == 'isChartsChildToDay') {
        this.isChartsChildToDay = true;
      } else if (chart == 'isChartsAttendanceChildToDay') {
        this.isChartsAttendanceChildToDay = true;
      } else if (chart == 'isChartsBudgetChildToDay') {
        this.isChartsBudgetChildToDay = true;
      } else if (chart == 'isChartsBudgetDifferenceChildToDay') {
        this.isChartsBudgetDifferenceChildToDay = true;
      }
    },
    closeModal: function closeModal(chart) {
      if (chart == 'isChartsChildToDay') {
        this.isChartsChildToDay = false;
      } else if (chart == 'isChartsAttendanceChildToDay') {
        this.isChartsAttendanceChildToDay = false;
      } else if (chart == 'isChartsBudgetChildToDay') {
        this.isChartsBudgetChildToDay = false;
      } else if (chart == 'isChartsBudgetDifferenceChildToDay') {
        this.isChartsBudgetDifferenceChildToDay = false;
      }
    },
    exportToExcel: function exportToExcel() {
      var _this10 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5() {
        var res, wb, ws, prop, worksheet, wscols, range, R, row, C, cell, cell_ref;
        return _regeneratorRuntime().wrap(function _callee5$(_context5) {
          while (1) switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return axios.get('api/get-data-export?start_date=' + _this10.startDate + '-01&end_date=' + _this10.endDate + '-01&government=' + _this10.selectedRegion + '&page=' + _this10.currentPage);
            case 2:
              res = _context5.sent;
              _this10.dataToExport = res.data;
              console.log(_this10.dataToExport);
              wb = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.table_to_book(document.querySelector(".c_d_table"));
              ws = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.json_to_sheet(_this10.dataToExport);
              prop = {
                '!rows': [],
                '!cols': []
              };
              worksheet = Object.assign(ws, prop);
              wb.Sheets.Sheet1 = worksheet;
              wscols = [{
                wch: 50
              }, {
                wch: -1
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }, {
                wch: 15
              }];
              range = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.decode_range(wb.Sheets.Sheet1['!ref']);
              wb.Sheets.Sheet1["!cols"] = wscols;
              for (R = range.s.r; R <= range.e.r; ++R) {
                row = {
                  level: 0
                };
                wb.Sheets.Sheet1['!rows'].push(row);
                for (C = range.s.c; C <= range.e.c; ++C) {
                  cell = {
                    c: C,
                    r: R
                  };
                  cell_ref = xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.utils.encode_cell(cell);
                  if (wb.Sheets.Sheet1[cell_ref]) {
                    wb.Sheets.Sheet1[cell_ref].s = {
                      font: {
                        name: "Calibri",
                        sz: 10,
                        color: {
                          rgb: "000000"
                        }
                      },
                      border: {
                        top: {
                          style: "thin",
                          color: {
                            rgb: "black"
                          }
                        },
                        bottom: {
                          style: "thin",
                          color: {
                            rgb: "black"
                          }
                        },
                        left: {
                          style: "thin",
                          color: {
                            rgb: "black"
                          }
                        },
                        right: {
                          style: "thin",
                          color: {
                            rgb: "black"
                          }
                        }
                      },
                      alignment: {
                        vertical: "center",
                        horizontal: "center",
                        wrapText: true
                      }
                    };
                  }
                  if (C == 1 && wb.Sheets.Sheet1['!rows'][R]) {
                    wb.Sheets.Sheet1['!rows'][R].level = wb.Sheets.Sheet1[cell_ref].v;
                  }
                }
              }
              if (!(_this10.currentPage == 'amount')) {
                _context5.next = 18;
                break;
              }
              return _context5.abrupt("return", xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.writeFile(wb, "\u0422\u0430\u0431\u043B\u0438\u0446\u0430 \u043F\u043E \u043F\u043E\u0441\u0435\u0449\u0435\u043D\u0438\u044F\u043C.xlsx"));
            case 18:
              if (!(_this10.currentPage == 'budget')) {
                _context5.next = 20;
                break;
              }
              return _context5.abrupt("return", xlsx_js_style__WEBPACK_IMPORTED_MODULE_6__.writeFile(wb, "\u0422\u0430\u0431\u043B\u0438\u0446\u0430 \u043F\u043E \u0431\u044E\u0434\u0436\u0435\u0442\u0443.xlsx"));
            case 20:
            case "end":
              return _context5.stop();
          }
        }, _callee5);
      }))();
    },
    formatter: function formatter(item, monthNumber, index) {
      if (item.data[monthNumber]) {
        if (index % 2 === 0) {
          return this.currentPage == 'amount' ? item.data[monthNumber].plan_child_to_day.toLocaleString(undefined, {
            maximumFractionDigits: 1
          }) : (item.data[monthNumber].budget_plan / 1000).toLocaleString(undefined, {
            maximumFractionDigits: 1
          });
        } else {
          return this.currentPage == 'amount' ? item.data[monthNumber].fact_child_to_day.toLocaleString(undefined, {
            maximumFractionDigits: 1
          }) : (item.data[monthNumber].budget_fact / 1000).toLocaleString(undefined, {
            maximumFractionDigits: 1
          });
        }
      }
    },
    classOfAttribute: function classOfAttribute(item, index) {
      if (index % 2 === 0) {
        return "child_to_day_month_plan_" + item.level;
      } else {
        return "child_to_day_month_fact_" + item.level;
      }
    },
    budgetGraph: function budgetGraph() {
      if (this.currentPage == 'budget') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    amountGraph: function amountGraph() {
      if (this.currentPage == 'amount') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    graphClass: function graphClass() {
      if (this.currentComponent == 'graph') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    tableClass: function tableClass() {
      if (this.currentComponent == 'table') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    changeBudgetToAmount: function changeBudgetToAmount(page) {
      this.currentPage = page;
    },
    changeTableToGraph: function changeTableToGraph(component) {
      this.currentComponent = component;
    },
    putInsideParentElement: function putInsideParentElement(obj, newObj, level, itemId, isExpend) {
      var vm = this;
      if (obj) {
        for (var prop in obj) {
          var o = obj[prop];
          if (o.child) {
            o.level = level;
            o.leaf = false;
            if (o.id == itemId) {
              o.expend = isExpend;
            }
            newObj.push(o);
            if (o.expend == true) {
              vm.putInsideParentElement(o.child, newObj, o.level + 1, itemId, isExpend);
            }
          } else {
            o.level = level;
            o.leaf = true;
            newObj.push(o);
          }
        }
      }
    },
    classByLevel: function classByLevel(item, index) {
      if (index === this.selectedRowIndex) {
        return "clicked_row";
      }
      for (var i = 0; i < 8; i++) {
        if (item.level == i) {
          return "table_row_level_" + i;
        }
      }
    },
    setClassOnclick: function setClassOnclick(item, index, event) {
      this.clickedGroup = item;
      this.clickedKindergarten = item;
      if (this.clickedGroup) {
        if (this.selectedRowIndex !== index) {
          var _circleElement = event.target.closest('table').querySelector(".green-circle");
          if (_circleElement) {
            _circleElement.remove();
          }
          var clickedRows = event.target.closest('table').querySelectorAll(".clicked_row");
          if (clickedRows) {
            clickedRows.forEach(function (td) {
              td.classList.remove("clicked_row");
            });
          }
        }
        this.selectedTitle = event.currentTarget.querySelector(".child_to_day_table_name_title").textContent;
        var trElement = event.target.closest('tr');
        var tds = trElement.querySelectorAll('td');
        tds.forEach(function (td) {
          td.classList.add("clicked_row");
        });
        this.selectedRowIndex = index;
        var firstTdElement = event.currentTarget.querySelector("td:first-child");
        var circleElement = firstTdElement.querySelector(".green-circle");
        if (!circleElement) {
          circleElement = document.createElement("div");
          circleElement.classList.add("green-circle");
          circleElement.style.width = "10px";
          circleElement.style.height = "10px";
          circleElement.style.borderRadius = "50%";
          circleElement.style.backgroundColor = "green";
          firstTdElement.appendChild(circleElement);
        }
      }
    },
    toggle: function toggle(item) {
      var vm = this;
      vm.itemId = item.id;
      this.clicked(item);
      var key = item.name;
      this.chartDataFiltered = {};
      this.chartAttendanceDataFiltered = {};
      this.chartData[key] = item;
      this.chartAttendanceDataFiltered[key] = item;
      item.leaf = false;
      if (item.leaf == false && item.expend == undefined && item.child != undefined) {
        if (item.child.length != 0) {
          vm.putInsideParentElement(item.child, [], item.level + 1, item.id, true);
        }
      }
      if (item.expend == true && item.child != undefined) {
        var __subindex = 0;
        for (var _key in item.child) {
          item.child[_key].expend = false;
        }
        ;
        vm.$set(item, "expend", undefined);
        vm.$set(item, "leaf", false);
        vm.itemId = null;
      }
    },
    setPadding: function setPadding(item) {
      return "padding-left: ".concat(item.level * 35, "px;");
    },
    iconName: function iconName(item) {
      if (item.expend == true) {
        return "arrow-up";
      }
      if (item.expend == undefined && item.child && item.child.length == 0) {
        return "done";
      }
      if (!item.child) {
        return "done";
      }
      return "arrow-down";
    },
    monthFormatter: function monthFormatter(month) {
      var dateParts = month.split('.');
      var date = new Date(dateParts[2], dateParts[1], dateParts[0]);
      var options = {
        month: 'long',
        timeZone: 'UTC',
        locale: 'ru'
      };
      return new Intl.DateTimeFormat('ru-RU', options).format(date).toLocaleUpperCase();
    },
    clicked: function clicked(item) {
      if (item.id == this.itemId) {
        return "clicked_row";
      }
    },
    updateData: function updateData() {
      var _this11 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6() {
        return _regeneratorRuntime().wrap(function _callee6$(_context6) {
          while (1) switch (_context6.prev = _context6.next) {
            case 0:
              _context6.next = 2;
              return axios.get('api/insert-to-table-analytics?start_date=' + _this11.startDate + '-01&end_date=' + _this11.endDate + '-01');
            case 2:
              _this11.mainData = _context6.sent;
            case 3:
            case "end":
              return _context6.stop();
          }
        }, _callee6);
      }))();
    },
    getData: function getData() {
      var _this12 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7() {
        var filteredBySearch;
        return _regeneratorRuntime().wrap(function _callee7$(_context7) {
          while (1) switch (_context7.prev = _context7.next) {
            case 0:
              _context7.next = 2;
              return axios.get('api/get-data-child-to-day?start_date=' + _this12.startDate + '-01&end_date=' + _this12.endDate + '-01&government=' + _this12.selectedRegion);
            case 2:
              _this12.mainData = _context7.sent;
              if (_this12.searchKindergarten != '') {
                _this12.selectedKindergarten = [];
                Object.values(_this12.mainData.data).forEach(function (item) {
                  _this12.checkInsideElement(item);
                });
                filteredBySearch = _this12.selectedKindergarten;
              } else {
                filteredBySearch = _this12.mainData.data;
              }
              _this12.childToDayFiltered = filteredBySearch;
              _this12.chartDataFiltered = filteredBySearch;
              _this12.chartAttendanceDataFiltered = filteredBySearch;
            case 7:
            case "end":
              return _context7.stop();
          }
        }, _callee7);
      }))();
    },
    fetchOptions: function fetchOptions() {
      var _this13 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8() {
        var startKey, endKey;
        return _regeneratorRuntime().wrap(function _callee8$(_context8) {
          while (1) switch (_context8.prev = _context8.next) {
            case 0:
              startKey = new Date(_this13.startDate).getMonth();
              endKey = new Date(_this13.endDate).getMonth();
              _this13.filteredMonthKeys = {};
              Object.keys(_this13.listOfMonthKeys).forEach(function (key) {
                if (startKey <= key && key <= endKey) {
                  _this13.$set(_this13.filteredMonthKeys, key, _this13.listOfMonthKeys[key]);
                }
              });
              _this13.clickedGroup = '';
              _context8.next = 7;
              return _this13.updateData();
            case 7:
              _this13.getData();
            case 8:
            case "end":
              return _context8.stop();
          }
        }, _callee8);
      }))();
    },
    checkInsideElement: function checkInsideElement(item) {
      var _this14 = this;
      if (Object.keys(item).length != 0 && Object.values(item)[0].length != 0) {
        Object.entries(item.child).reduce(function (acc, _ref3) {
          var _ref4 = _slicedToArray(_ref3, 2),
            key = _ref4[0],
            value = _ref4[1];
          if (value['child']) {
            Object.keys(value['child']).forEach(function (key) {
              _this14.checkInsideElement(value['child'][key]);
            });
          } else {
            if (value.name.toLowerCase().includes(_this14.searchKindergarten.toLowerCase())) {
              _this14.selectedKindergarten.push(value);
            }
          }
        }, {});
      }
    }
  },
  created: function created() {
    var _this15 = this;
    return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee9() {
      return _regeneratorRuntime().wrap(function _callee9$(_context9) {
        while (1) switch (_context9.prev = _context9.next) {
          case 0:
            _this15.endDate = _this15.currentDate;
            _this15.getRegions();
            _context9.next = 4;
            return _this15.updateData();
          case 4:
            _this15.getData();
            window.addEventListener('resize', _this15.updateScreenWidth);
          case 6:
          case "end":
            return _context9.stop();
        }
      }, _callee9);
    }))();
  },
  beforeUnmount: function beforeUnmount() {
    window.removeEventListener('resize', this.updateScreenWidth);
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _public_images_icon_close_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../public/images/icon_close.svg */ "./public/images/icon_close.svg");
/* harmony import */ var _public_images_icon_close_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_public_images_icon_close_svg__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_images_fullscreen_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../public/images/fullscreen.svg */ "./public/images/fullscreen.svg");
/* harmony import */ var _public_images_fullscreen_svg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_public_images_fullscreen_svg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _public_images_shortscreen_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../public/images/shortscreen.svg */ "./public/images/shortscreen.svg");
/* harmony import */ var _public_images_shortscreen_svg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_public_images_shortscreen_svg__WEBPACK_IMPORTED_MODULE_4__);
// Imports





var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_icon_close_svg__WEBPACK_IMPORTED_MODULE_2___default()));
var ___CSS_LOADER_URL_REPLACEMENT_1___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_fullscreen_svg__WEBPACK_IMPORTED_MODULE_3___default()));
var ___CSS_LOADER_URL_REPLACEMENT_2___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_shortscreen_svg__WEBPACK_IMPORTED_MODULE_4___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".blind_title[data-v-3239cd30] {\n    font-size: 32px; \n    color: #5F46C5;\n}\n.stats_groups select[data-v-3239cd30] {\n    margin-left: 15px;\n    padding: 3px 20px;\n    border: 1px solid #dadada;\n    border-radius: 8px;\n}\n.stats_month[data-v-3239cd30] {\n    font-size: 0.875rem;\n    padding: 5px 10px;\n    border: 1px solid #DADADA;\n    border-radius: 10px;\n}\n.digital[data-v-3239cd30] {\n  width: 180px;\n  font-style: normal;\n  font-weight: 400;\n  line-height: 42px;\n  text-align: center;\n}\n.digital__time[data-v-3239cd30] {\n  font-size: 42px;\n  color: #666666;\n  display: inline-block;\n  text-shadow: 1px 1px 1px rgba(0,0,0,0.4);\n}\n.digital__milliseconds[data-v-3239cd30] {\n  font-size: 8px;\n}\n.blind_logo[data-v-3239cd30] {\n    width: 100%;\n    height: 18%;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    padding: 10px 5px;\n    background-color: #fff;\n    border-radius: 20px;\n}\n.blind_center_count[data-v-3239cd30]  {\n    width: 100%;\n    background: #fff;\n    height: 77%;\n    border-radius: 20px;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: space-evenly;\n}\n.scroll_style[data-v-3239cd30]::-webkit-scrollbar-track {\n    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);\n    border-radius: 10px;\n    background-color: #F5F5F5;\n}\n.scroll_style[data-v-3239cd30]::-webkit-scrollbar {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    background-color: #F5F5F5;\n}\n.scroll_style[data-v-3239cd30]::-webkit-scrollbar-thumb {\n    border-radius: 10px;\n    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);\n    background-color: #555;\n}\n.arrow[data-v-3239cd30] {\n  width: 50px;\n  position: absolute;\n  bottom: 30px;\n  left: calc(50% - 25px);\n  animation: arrowUp-data-v-3239cd30 2s infinite;\n}\n.arrow_page[data-v-3239cd30] {\n  width: 30px;\n  position: absolute;\n  left: calc(50% - 25px);\n  animation: arrowDown-data-v-3239cd30 2s infinite;\n}\n.arrow.two[data-v-3239cd30] {\n  opacity: 0;\n  animation-delay: 1s;\n}\n.arrow_page.two_page[data-v-3239cd30] {\n  opacity: 0;\n  animation-delay: 1s;\n}\n@keyframes arrowUp-data-v-3239cd30 {\n0% {\n    bottom: 10px;\n    opacity: 1;\n}\n100% {\n    bottom: 30px;\n    opacity: 0;\n}\n}\n@keyframes arrowDown-data-v-3239cd30 {\n0% {\n    bottom: 30px;\n    opacity: 0;\n}\n100% {\n    bottom: 0px;\n    opacity: 1;\n}\n}\n.drag_button_wrapper[data-v-3239cd30]{\n    position: absolute;\n    top: 97.5%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n}\n.drag_button_page[data-v-3239cd30] {\n    cursor: pointer;\n    position: absolute;\n    top: 2.5%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n    width: 20px;\n    height: 80px;\n}\n.drag_button[data-v-3239cd30] {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    color: #fff;\n    cursor: grab;\n    transition: transform 0.3s;\n}\n.drag_button_active[data-v-3239cd30] {\n  transform: translateY(-10px);\n  cursor: grabbing;\n}\n.drag_button span[data-v-3239cd30] {\n  font-size: 16px;\n  font-weight: bold;\n}\n.blind_center[data-v-3239cd30] {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n}\n.blind_region[data-v-3239cd30] {\n    font-style: normal;\n    font-weight: 400;\n    font-size: 18px;\n    line-height: 28px;\n    color: #666666;\n    text-align: center;\n    margin-top: 25px;\n}\n.exactly_date[data-v-3239cd30] {\n    font-style: normal;\n    font-weight: 400;\n    font-size: 36px;\n    line-height: 42px;\n    color: #666666;\n    text-shadow: 1px 1px 1px rgba(0,0,0,0.4);\n}\n.blind_chart[data-v-3239cd30] {\n    height: 100%;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: space-around;\n}\n.donut_number[data-v-3239cd30] {\n    font-style: normal;\n    font-weight: 700;\n    font-size: 42px;\n    line-height: 49px;\n    letter-spacing: 0.01em;\n}\n.donut_info[data-v-3239cd30] {\n    font-style: normal;\n    font-weight: 400;\n    font-size: 24px;\n    line-height: 28px;\n    letter-spacing: 0.01em;\n}\n.donut_sick_color[data-v-3239cd30]{\n    color: #EDA774;\n}\n.donut_weekend_color[data-v-3239cd30]{\n    color: #4F81BD;\n}\n.donut_missed_color[data-v-3239cd30]{\n    color: #B1B1B7;\n}\n.donut_description[data-v-3239cd30] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n}\n#autoScrollTrigger[data-v-3239cd30] {\n    height: 100vh;\n    position: absolute;\n    top: 0;\n    z-index: 99;\n    width: 100%;\n    background: rgba(0, 0, 0, 0.55);\n    -webkit-backdrop-filter: blur(5px);\n            backdrop-filter: blur(5px);\n    transition: height 0.5s ease;\n}\n.blind_description[data-v-3239cd30] {\n    font-style: normal;\n    font-weight: 400;\n    font-size: 20px;\n    line-height: 28px;\n    text-align: center;\n    color: #949494;\n}\n.current_day_blind[data-v-3239cd30] {\n    display: flex;\n    justify-content: space-between;\n    flex-direction: column;\n    align-items: center;\n    height: 80%;\n    width: 30%;\n}\n.numbers_blind[data-v-3239cd30] {\n    font-style: normal;\n    font-weight: 700;\n    font-size: 100px;\n    line-height: 80px;\n    text-align: center;\n}\n.blind_wrapper[data-v-3239cd30] {\n    height: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: space-evenly;\n}\n.blind_card[data-v-3239cd30] {\n    width: 30%;\n    height: 80%;\n    box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.2);\n    border-radius: 20px;\n    padding: 60px 45px;\n    background-color: #fff;\n}\n.show_blind[data-v-3239cd30] {\n    height: 100vh;\n}\n.hide_blind[data-v-3239cd30] {\n    height: 0vh;\n}\n.cells[data-v-3239cd30] {\n    display: flex;\n    justify-content: center;\n}\n.tarify_table[data-v-3239cd30] {\n    position: fixed;\n    z-index: 9998;\n    top: 10vh;\n    right: 0.5%;\n    width: 38.3%;\n    height: 40%;\n}\n.hide_none[data-v-3239cd30] {\n    display: none;\n}\n.show_table[data-v-3239cd30] {\n    display: block !important;\n}\n.last_downloaded_date[data-v-3239cd30]{\n    font-weight: 400;\n    font-size: 14px;\n    line-height: 14px;\n    display: flex;\n    align-items: center;\n    text-align: right;\n    letter-spacing: 0.01em;\n    color: #97B0BF;\n}\n.kindergarten_filter[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n}\n.modal_table[data-v-3239cd30] {\n    position: fixed;\n    z-index: 9998;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0, 0, 0, 0.5);\n    display: table;\n    transition: opacity 0.3s ease;\n}\n.modal_container_table[data-v-3239cd30] {\n    position: relative;\n    top: 7vh;\n    width: 99vw;\n    height: 91vh;   \n    margin: 0.5%;\n    background-color: #fff;\n    border-radius: 10px;\n    overflow: hidden;\n    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.33);\n    transition: all 0.3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.modal_container_tarify[data-v-3239cd30] {\n    position: relative;\n    top: 7vh;\n    width: 100%;\n    height: 100%;\n    margin: 0.5%;\n    background-color: #fff;\n    border-radius: 10px;\n    overflow: hidden;\n    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.33);\n    transition: all 0.3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.modal_graph[data-v-3239cd30] {\n    position: fixed;\n    z-index: 9998;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0, 0, 0, 0.5);\n    display: table;\n    transition: opacity 0.3s ease;\n}\n.modal_attendance[data-v-3239cd30] {\n    position: relative;\n    top: 15vh;\n    right: -34%;\n    width: 550px;\n    height: 81vh;   \n    margin: 0.5%;\n    background-color: #fff;\n    border-radius: 10px;\n    overflow: hidden;\n    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.33);\n    transition: all 0.3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.modal_container[data-v-3239cd30] {\n    position: relative;\n    top: 16vh;\n    width: 99%;\n    height: 81vh;   \n    margin: 0.5%;\n    background-color: #fff;\n    border-radius: 10px;\n    overflow: hidden;\n    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.33);\n    transition: all 0.3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.close[data-v-3239cd30] {\n    cursor: pointer;\n    width: 20px;\n    height: 20px;\n    background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n    background-position: center;\n    background-repeat: no-repeat;\n}\n.fullscreen[data-v-3239cd30] {\n    cursor: pointer;\n    width: 20px;\n    height: 20px;\n    background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ");\n    background-position: center;\n    background-repeat: no-repeat;\n}\n.shortscreen[data-v-3239cd30] {\n    cursor: pointer;\n    width: 20px;\n    height: 20px;\n    background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_2___ + ");\n    background-position: center;\n    background-repeat: no-repeat;\n}\n.child_to_day_parameters[data-v-3239cd30] {\n    height: 7vh;\n    display: flex;\n    color: black;\n    flex-direction: row;\n    align-items: center;\n    justify-content: space-between;\n    margin: 0.5%;\n    border-radius: 10px;\n    box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.2);\n    font-size: 14px;\n}\n.child_to_day_parameter[data-v-3239cd30] {\n    height: 100%;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n}\n.child_to_day_select[data-v-3239cd30] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n}\n.child_to_day_select_graphs[data-v-3239cd30] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    border-radius: 5px;\n    border: 1px solid #5F46C5;\n}\n.child_to_day_select_graph[data-v-3239cd30] {\n    padding: 5px;\n    cursor: pointer;\n}\n.child_to_day_select_graph_clicked[data-v-3239cd30] {\n    background-color: #5F46C5;\n    color: white;\n}\n.child_to_day[data-v-3239cd30] {\n    display: flex;\n    flex-direction: row;\n    height: 82vh;\n}\n.child_to_day_table[data-v-3239cd30] {\n    position: relative;\n    width: 60%;\n    height: 98%;\n    margin: 0.5%;\n    border-radius: 10px;\n    padding-bottom: 9px;\n    overflow: hidden;\n    box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.2);\n}\n.table[data-v-3239cd30] {\n    border: 1px solid #e5eaef;\n}\n.search_table[data-v-3239cd30] {\n    border: 1px solid #e5eaef;\n    padding: 5px 10px;\n    margin: 10px 20px;\n}\n.child_to_day_graphs[data-v-3239cd30] {\n    height: 100%;\n    width: 38%;\n    margin: 0.5%;\n}\n.child_to_day_graph[data-v-3239cd30] {\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    height: 48.5%;\n    border-radius: 10px;\n    overflow: hidden;\n    box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.2);\n}\n.child_to_day_header[data-v-3239cd30] {       \n    font-size: 12px;\n    color: #6087A0;\n    font-weight: 600;\n    text-align: center;\n    vertical-align: inherit;\n    border: 1px solid #e5eaef;\n    padding: 5px;\n    line-height: 14px;\n    background: white;\n    text-transform: uppercase;\n}\n.child_to_day_header_name[data-v-3239cd30] {\n    min-width: 350px !important;\n}\n.child_to_day_header_amount[data-v-3239cd30] {\n    min-width: 75px !important;\n    max-width: 125px !important;\n}\n.child_to_day_border[data-v-3239cd30] {\n    font-size: 12px;\n    color: black;\n    text-align: center;\n    padding: 10px;\n    border: 1px solid #e5eaef;\n}\n.child_to_day_header_empty[data-v-3239cd30] {\n    max-width: 40px !important;\n    min-width: 30px !important;\n}\n.text_left[data-v-3239cd30] {\n    text-align: left;\n}\n.child_to_day_table_name[data-v-3239cd30] {\n    cursor: pointer;\n    min-width: 36%;\n    padding: 0;\n    border-left: none !important;\n}\n.child_to_day_table_name_block[data-v-3239cd30] {\n    display: flex;\n    flex-wrap: nowrap;\n    align-items: center;\n    height: 100%;\n}\n.child_to_day_table_name_title[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    height: 100%;\n    border-left: 1px solid #e5eaef;\n    padding: 0 10px;\n}\n.child_to_day_table_numbers[data-v-3239cd30] {\n    min-width: 75px !important;\n    max-width: 125px !important;\n}\n.child_to_day_month_plan_0[data-v-3239cd30] {\n    background-color: #80b4e4;\n}\n.child_to_day_month_fact_0[data-v-3239cd30] {\n    background-color: #bbd1e6;\n}\n.child_to_day_month_plan_1[data-v-3239cd30] {\n    background-color: #a7e0fb;\n}\n.child_to_day_month_fact_1[data-v-3239cd30] {\n    background-color: #c7eafb;\n}\n.child_to_day_month_plan_2[data-v-3239cd30] {\n    background-color: #b5e2f7;\n}\n.child_to_day_month_fact_2[data-v-3239cd30] {\n    background-color: #bfe4f8;\n}\n.child_to_day_month_plan_3[data-v-3239cd30] {\n    background-color: #ccecff;\n}\n.child_to_day_month_fact_3[data-v-3239cd30] {\n    background-color: #daecf7;\n}\n.child_to_day_month_plan_4[data-v-3239cd30] {\n    background-color: #c9ebd7;\n}\n.child_to_day_month_fact_4[data-v-3239cd30] {\n    background-color: #f9f6dd;\n}\n.arrow-up[data-v-3239cd30] {\n    width: 20px;\n    height: 20px;\n    background-image: url(\"/images/arrow-up.png\");\n    background-position: center;\n    background-repeat: no-repeat;\n}\n.arrow-down[data-v-3239cd30] {\n    width: 20px;\n    height: 20px;\n    background-image: url(\"/images/arrow-down.png\");\n    background-position: center;\n    background-repeat: no-repeat;\n}\n.child_to_day_footer[data-v-3239cd30] {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-end;\n    align-items: center;\n    padding: 10px;\n    background: #E8F4FC;\n}\n.child_to_day_head_formula[data-v-3239cd30] {\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: flex-start;\n    padding: 10px 20px;\n    background: #E8F4FC;\n}\n.child_to_day_head[data-v-3239cd30] {\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    padding: 10px 20px;\n    background: #E8F4FC;\n}\n.child_to_day_title[data-v-3239cd30] {\n    font-weight: 700;\n    font-size: 0.875rem;\n    line-height: 17px;\n    display: flex;\n    align-items: center;\n    letter-spacing: 0.01em;\n    text-transform: uppercase;\n    color: #3F4D67;\n}\n.child_to_day_unit[data-v-3239cd30] {\n    font-weight: 400;\n    font-size: 14px;\n    line-height: 16px;\n    display: flex;\n    align-items: center;\n    text-align: right;\n    letter-spacing: 0.01em;\n    color: #6087A0;\n}\n.child_to_day_search[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n}\n.c_d_table[data-v-3239cd30]{\n  max-width: 100%;\n  height: 85.5%;\n  overflow: scroll;\n}\n.c_table[data-v-3239cd30], .table_child[data-v-3239cd30] {\n  max-width: 100%;\n  border-collapse: separate;\n  border-spacing: 0;\n  max-height: 100%;\n}\n.table_child thead[data-v-3239cd30] {\n  position: sticky;\n  top: 0;\n}\n.clicked_row[data-v-3239cd30] {\n    background: #b9e0dd !important;\n}\n.table_row_level_0[data-v-3239cd30] {\n    background: #9ec3e6;\n}\n.table_row_level_1[data-v-3239cd30] {\n    background: #b5e2f7;\n}\n.table_row_level_2[data-v-3239cd30] {\n    background: #c0e6ff;\n}\n.table_row_level_3[data-v-3239cd30] {\n    background: #dff3ff;\n}\n.table_row_level_4[data-v-3239cd30] {\n    background: white;\n}\n.relax_day[data-v-3239cd30] {\n    background: #ddf1fa;\n}\n.edited_missing_day[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    min-width: 20px;\n    height: 20px;\n    padding: 5px;\n    border: 1px dashed #b1b1b7;\n    border-radius: 3px;\n    background: #efeff1;\n    vertical-align: inherit;\n}\n.missing_day[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    min-width: 20px;\n    height: 20px;\n    padding: 5px;\n    border-radius: 3px;\n    background: #b1b1b7;\n    vertical-align: inherit;\n}\n.sick_day[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    min-width: 20px;\n    height: 20px;\n    padding: 5px;\n    border-radius: 3px;\n    background: #eda774;\n    vertical-align: inherit;\n}\n.edited_sick_day[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    min-width: 20px;\n    height: 20px;\n    padding: 5px;\n    border: 1px dashed #eda774;\n    border-radius: 3px;\n    background: #fbede3;\n    vertical-align: inherit;\n}\n.wekeend_day[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    min-width: 20px;\n    height: 20px;\n    padding: 5px;\n    border-radius: 3px;\n    background: #4f81bd;\n    vertical-align: inherit;\n}\n.edited_wekeend_day[data-v-3239cd30] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    min-width: 20px;\n    height: 20px;\n    padding: 5px;\n    border: 1px dashed #4f81bd;\n    border-radius: 3px;\n    background: #dce6f2;\n    vertical-align: inherit;\n}\n.attended_day[data-v-3239cd30] {\n  cursor: pointer;\n  min-width: 20px;\n  height: 20px;\n  padding: 5px;\n}\n.edited_attended_day[data-v-3239cd30] {\n  cursor: pointer;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  min-width: 20px;\n  height: 20px;\n  padding: 5px;\n  border: 1px dashed black;\n  border-radius: 3px;\n  background: #fff;\n  vertical-align: inherit;\n}\n.missing_circle[data-v-3239cd30] {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    background: #b1b1b7;\n    border-radius: 50%;\n}\n.edited_missing_circle[data-v-3239cd30] {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    border: 1px dashed #b1b1b7;\n    background: #efeff1;\n    border-radius: 50%;\n}\n.sick_circle[data-v-3239cd30] {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    background: #eda774;\n    border-radius: 50%;\n}\n.edited_sick_circle[data-v-3239cd30] {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    border: 1px dashed #eda774;\n    background: #fbede3;\n    border-radius: 50%;\n}\n.wekeend_circle[data-v-3239cd30] {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    background: #4f81bd;\n    border-radius: 50%;\n}\n.edited_wekeend_circle[data-v-3239cd30] {\n    cursor: pointer;\n    width: 10px;\n    height: 10px;\n    border: 1px dashed #4f81bd;\n    background: #dce6f2;\n    border-radius: 50%;\n}\n.dashboard_select_government[data-v-3239cd30] {\n    width: 300px;\n}\n#modal-2[data-v-3239cd30] {\n    width: 300px;\n}\n@media screen and (max-width: 1280px) {\n.child_to_day[data-v-3239cd30] {\n        height: 100vh;\n        flex-direction: column-reverse;\n}\n.child_to_day_graphs[data-v-3239cd30] {\n        display: flex;\n        justify-content: space-between;\n        height: 100%;\n        width: 99%;\n}\n.child_to_day_graph[data-v-3239cd30] {\n        height: 100%;\n        width: 49%;\n}\n.child_to_day_table[data-v-3239cd30] {\n        width: 99%;\n}\n.dashboard_select_government[data-v-3239cd30] {\n        width: 250px;\n}\n}\n@media screen and (max-width: 1024px) {\n.child_to_day_parameters_md[data-v-3239cd30] {\n        display: flex;\n        justify-content: space-between;\n        align-content: center;\n        align-items: center;\n        padding: 10px 20px;\n}\n.child_to_day_select_graphs[data-v-3239cd30] {\n        width: 100%;\n}\n.child_to_day_select_graph[data-v-3239cd30] {\n        width: 50%;\n}\n.child_to_day_select[data-v-3239cd30] {\n        width: 100%;\n}\n.dashboard_select_government[data-v-3239cd30] {\n        width: 100% !important;\n}\n.modal_attendance[data-v-3239cd30] {\n        width: 80%;\n        height: 80%;\n        top: 50%;\n        left: 50%;\n        transform: translate(-50%, -50%);\n}\n}\n@media screen and (max-width: 768px) {\n.child_to_day_graphs[data-v-3239cd30] {\n        flex-direction: column;\n}\n.child_to_day_graph[data-v-3239cd30] {\n        width: 99%;\n        height: 500px !important;\n}\n}\n@media screen and (max-width: 414px) {\n}\n\n    \n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./public/images/fullscreen.svg":
/*!**************************************!*\
  !*** ./public/images/fullscreen.svg ***!
  \**************************************/
/***/ ((module) => {

module.exports = "/images/fullscreen.svg?5c09e363887b25afdadfce912842e9d6";

/***/ }),

/***/ "./public/images/shortscreen.svg":
/*!***************************************!*\
  !*** ./public/images/shortscreen.svg ***!
  \***************************************/
/***/ ((module) => {

module.exports = "/images/shortscreen.svg?685ebec1906930eb252e48aaf683957d";

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_child_to_day_css_vue_type_style_index_0_id_3239cd30_scoped_true_lang_css_external__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_child_to_day_css_vue_type_style_index_0_id_3239cd30_scoped_true_lang_css_external__WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_child_to_day_css_vue_type_style_index_0_id_3239cd30_scoped_true_lang_css_external__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/views/dashboard/index.vue":
/*!************************************************!*\
  !*** ./resources/js/views/dashboard/index.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_3239cd30_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=3239cd30&scoped=true */ "./resources/js/views/dashboard/index.vue?vue&type=template&id=3239cd30&scoped=true");
/* harmony import */ var _child_to_day_js_vue_type_script_lang_js_external__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./child_to_day.js?vue&type=script&lang=js&external */ "./resources/js/views/dashboard/child_to_day.js?vue&type=script&lang=js&external");
/* harmony import */ var _child_to_day_css_vue_type_style_index_0_id_3239cd30_scoped_true_lang_css_external__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external */ "./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _child_to_day_js_vue_type_script_lang_js_external__WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_3239cd30_scoped_true__WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_3239cd30_scoped_true__WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "3239cd30",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/child_to_day.js?vue&type=script&lang=js&external":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/dashboard/child_to_day.js?vue&type=script&lang=js&external ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_child_to_day_js_vue_type_script_lang_js_external__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./child_to_day.js?vue&type=script&lang=js&external */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./resources/js/views/dashboard/child_to_day.js?vue&type=script&lang=js&external");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_child_to_day_js_vue_type_script_lang_js_external__WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external ***!
  \************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_child_to_day_css_vue_type_style_index_0_id_3239cd30_scoped_true_lang_css_external__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/js/views/dashboard/child_to_day.css?vue&type=style&index=0&id=3239cd30&scoped=true&lang=css&external");


/***/ }),

/***/ "./resources/js/views/dashboard/index.vue?vue&type=template&id=3239cd30&scoped=true":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/dashboard/index.vue?vue&type=template&id=3239cd30&scoped=true ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3239cd30_scoped_true__WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   staticRenderFns: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3239cd30_scoped_true__WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3239cd30_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=3239cd30&scoped=true */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/dashboard/index.vue?vue&type=template&id=3239cd30&scoped=true");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/dashboard/index.vue?vue&type=template&id=3239cd30&scoped=true":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/dashboard/index.vue?vue&type=template&id=3239cd30&scoped=true ***!
  \*********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render),
/* harmony export */   staticRenderFns: () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "child_to_day_page" },
    [
      _c("Nav"),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            id: "modal-2",
            title: "Фильтр",
            "ok-title": "Применить",
            "cancel-title": "Отмена"
          },
          on: { ok: _vm.updatePage },
          model: {
            value: _vm.isFilter,
            callback: function($$v) {
              _vm.isFilter = $$v
            },
            expression: "isFilter"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "text-align-center",
              staticStyle: { width: "100%" }
            },
            [
              _c(
                "div",
                { staticClass: "child_to_day_select_graphs w-100 mb-3" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "child_to_day_select_graph px-3",
                      class: _vm.budgetGraph(),
                      on: {
                        click: function($event) {
                          return _vm.changeBudgetToAmount("budget")
                        }
                      }
                    },
                    [_vm._v(" Сумма ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "child_to_day_select_graph px-3",
                      class: _vm.amountGraph(),
                      on: {
                        click: function($event) {
                          return _vm.changeBudgetToAmount("amount")
                        }
                      }
                    },
                    [_vm._v(" Количество ")]
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "child_to_day_select mb-3" }, [
                _c("div", [_vm._v(" Период c ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.startDate,
                      expression: "startDate"
                    }
                  ],
                  staticClass: "mx-2 px-2",
                  attrs: {
                    type: "month",
                    name: "date_start",
                    max: _vm.endDate,
                    required: ""
                  },
                  domProps: { value: _vm.startDate },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.startDate = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", [_vm._v(" по ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.endDate,
                      expression: "endDate"
                    }
                  ],
                  staticClass: "mx-2 px-2",
                  attrs: {
                    type: "month",
                    name: "date_end",
                    min: _vm.startDate,
                    max: _vm.currentDate,
                    required: ""
                  },
                  domProps: { value: _vm.endDate },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.endDate = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _vm.regions
                ? _c("div", { staticClass: "child_to_day_select mb-3" }, [
                    _c("div", [_vm._v(" Регионы ")]),
                    _vm._v(" "),
                    _c(
                      "select",
                      {
                        staticClass:
                          "w-100 p-1 mx-1 dashboard_select_government",
                        on: {
                          "!change": function($event) {
                            return _vm.onChange($event)
                          }
                        }
                      },
                      _vm._l(_vm.regions, function(region, index) {
                        return _c(
                          "option",
                          {
                            key: index,
                            staticStyle: { padding: "5px" },
                            domProps: { value: region.id }
                          },
                          [_vm._v(" " + _vm._s(region.region) + " ")]
                        )
                      }),
                      0
                    )
                  ])
                : _vm._e()
            ]
          )
        ]
      ),
      _vm._v(" "),
      _vm.isLargeScreen
        ? _c("div", { staticClass: "child_to_day_parameters" }, [
            _c("div", { staticClass: "child_to_day_parameter" }, [
              _c("div", { staticClass: "child_to_day_parameter" }, [
                _c("div", { staticClass: "child_to_day_select_graphs mx-3" }, [
                  _c(
                    "div",
                    {
                      staticClass: "child_to_day_select_graph px-3",
                      class: _vm.budgetGraph(),
                      on: {
                        click: function($event) {
                          return _vm.changeBudgetToAmount("budget")
                        }
                      }
                    },
                    [_vm._v(" Сумма ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "child_to_day_select_graph px-3",
                      class: _vm.amountGraph(),
                      on: {
                        click: function($event) {
                          return _vm.changeBudgetToAmount("amount")
                        }
                      }
                    },
                    [_vm._v(" Количество ")]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "child_to_day_select" }, [
                _c("div", [_vm._v(" Период c ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.startDate,
                      expression: "startDate"
                    }
                  ],
                  staticClass: "mx-2 px-2",
                  attrs: {
                    type: "month",
                    name: "date_start",
                    max: _vm.endDate,
                    required: ""
                  },
                  domProps: { value: _vm.startDate },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.startDate = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", [_vm._v(" по ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.endDate,
                      expression: "endDate"
                    }
                  ],
                  staticClass: "mx-2 px-2",
                  attrs: {
                    type: "month",
                    name: "date_end",
                    min: _vm.startDate,
                    max: _vm.currentDate,
                    required: ""
                  },
                  domProps: { value: _vm.endDate },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.endDate = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _vm.regions
                ? _c(
                    "div",
                    {
                      staticClass: "child_to_day_select px-3 mr-3",
                      staticStyle: {
                        "border-left": "1px solid #b9b9b9",
                        "border-right": "1px solid #b9b9b9"
                      }
                    },
                    [
                      _c("div", [_vm._v(" Регионы ")]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          staticClass:
                            "w-100 p-1 mx-1 dashboard_select_government",
                          on: {
                            "!change": function($event) {
                              return _vm.onChange($event)
                            }
                          }
                        },
                        _vm._l(_vm.regions, function(region, index) {
                          return _c(
                            "option",
                            {
                              key: index,
                              staticStyle: { padding: "5px" },
                              domProps: { value: region.id }
                            },
                            [_vm._v(" " + _vm._s(region.region) + " ")]
                          )
                        }),
                        0
                      )
                    ]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "last_downloaded_date mx-3",
                staticStyle: { display: "flex", "flex-direction": "column" }
              },
              [
                _c(
                  "button",
                  {
                    staticClass: "last_downloaded_date",
                    staticStyle: {
                      border: "transparent",
                      "background-color": "transparent",
                      position: "relative",
                      top: "-1px"
                    },
                    on: {
                      click: function($event) {
                        return _vm.updateAttendance()
                      }
                    }
                  },
                  [_vm._v("  Обновить ")]
                )
              ]
            )
          ])
        : _vm._e(),
      _vm._v(" "),
      !_vm.isLargeScreen
        ? _c("div", { staticClass: "child_to_day_parameters_md" }, [
            _c("div", { staticClass: "flex align-items-center" }, [
              _c("div", { staticClass: "flex child_to_day_select_graphs" }, [
                _c(
                  "div",
                  {
                    staticClass: "child_to_day_select_graph px-3",
                    class: _vm.tableClass(),
                    on: {
                      click: function($event) {
                        return _vm.changeTableToGraph("table")
                      }
                    }
                  },
                  [_vm._v(" Таблица ")]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "child_to_day_select_graph px-3",
                    class: _vm.graphClass(),
                    on: {
                      click: function($event) {
                        return _vm.changeTableToGraph("graph")
                      }
                    }
                  },
                  [_vm._v(" Графики ")]
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "ml-3",
                  on: {
                    click: function($event) {
                      _vm.isFilter = true
                    }
                  }
                },
                [_vm._v(" Фильтр")]
              )
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "last_downloaded_date mx-3",
                staticStyle: { display: "flex", "flex-direction": "column" }
              },
              [
                _c(
                  "button",
                  {
                    staticClass: "last_downloaded_date large_text",
                    staticStyle: {
                      border: "transparent",
                      "background-color": "transparent",
                      position: "relative",
                      top: "-1px"
                    },
                    on: {
                      click: function($event) {
                        return _vm.updateAttendance()
                      }
                    }
                  },
                  [_vm._v("  Обновить ")]
                )
              ]
            )
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "child_to_day" }, [
        _vm.currentComponent == "table" || _vm.isLargeScreen
          ? _c("div", { staticClass: "child_to_day_table" }, [
              _c("div", { staticClass: "child_to_day_head" }, [
                _vm.currentPage == "amount"
                  ? _c("div", { staticClass: "child_to_day_title" }, [
                      _vm._v("План/Факт отметок")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.currentPage == "budget"
                  ? _c("div", { staticClass: "child_to_day_title" }, [
                      _vm._v("План/Факт бюджет")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.currentPage == "amount"
                  ? _c("div", { staticClass: "child_to_day_unit" }, [
                      _vm._v("количество")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.currentPage == "budget"
                  ? _c("div", { staticClass: "child_to_day_unit" }, [
                      _vm._v("тыс. тенге")
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "child_to_day_search" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.searchKindergarten,
                      expression: "searchKindergarten"
                    }
                  ],
                  staticClass: "search_table",
                  staticStyle: { "font-size": "14px", width: "30%" },
                  attrs: { type: "text", placeholder: "Поиск..." },
                  domProps: { value: _vm.searchKindergarten },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.searchKindergarten = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn mr-3",
                    staticStyle: {
                      "font-size": "12px",
                      "background-color": "#5F46C5",
                      color: "white",
                      "margin-right": "20px"
                    },
                    on: { click: _vm.exportToExcel }
                  },
                  [_vm._v(" Скачать в excel")]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "c_d_table scroll_style" }, [
                _c(
                  "table",
                  { staticClass: "c_table w-100", attrs: { id: "myTable" } },
                  [
                    _c("thead", [
                      _c(
                        "tr",
                        { staticClass: "center" },
                        [
                          _c("th", {
                            staticClass:
                              "child_to_day_header child_to_day_header_empty",
                            attrs: { rowspan: "2" }
                          }),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass:
                                "child_to_day_header child_to_day_header_name",
                              attrs: { rowspan: "2" }
                            },
                            [_vm._v(" НАИМЕНОВАНИЕ ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass:
                                "child_to_day_header child_to_day_header_amount",
                              attrs: { rowspan: "2" }
                            },
                            [_vm._v(" КОЛ-ВО ГРУПП ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass:
                                "child_to_day_header child_to_day_header_amount",
                              attrs: { rowspan: "2" }
                            },
                            [_vm._v(" ПЛАН. КОЛ-ВО ДЕТЕЙ ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass:
                                "child_to_day_header child_to_day_header_amount",
                              attrs: { rowspan: "2" }
                            },
                            [_vm._v(" ФАКТ. КОЛ-ВО ДЕТЕЙ ")]
                          ),
                          _vm._v(" "),
                          _vm._l(_vm.months["names"], function(monthName) {
                            return _c(
                              "th",
                              {
                                key: monthName,
                                staticClass: "child_to_day_header",
                                attrs: { colspan: "2", "data-th": monthName }
                              },
                              [
                                _vm._v(
                                  "\n                                " +
                                    _vm._s(monthName) +
                                    "\n                            "
                                )
                              ]
                            )
                          })
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c(
                        "tr",
                        _vm._l(_vm.months["attributes"], function(
                          monthAttribute,
                          index
                        ) {
                          return _c(
                            "th",
                            {
                              key: index,
                              staticClass: "child_to_day_header",
                              attrs: { "data-th": monthAttribute }
                            },
                            [
                              _vm._v(
                                "\n                                " +
                                  _vm._s(monthAttribute) +
                                  "\n                            "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.arrayTreeObj, function(item, index) {
                        return _c(
                          "tr",
                          {
                            key: index,
                            class: _vm.classByLevel(item, index),
                            staticStyle: { height: "1px" },
                            on: {
                              click: function($event) {
                                return _vm.setClassOnclick(item, index, $event)
                              }
                            }
                          },
                          [
                            _c("td", {
                              staticClass: "child_to_day_border",
                              staticStyle: { width: "30px" }
                            }),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass:
                                  "child_to_day_border child_to_day_table_name text_left",
                                staticStyle: {
                                  "text-align": "left",
                                  height: "inherit"
                                },
                                attrs: { "data-th": "Названия строк" },
                                on: {
                                  click: function($event) {
                                    return _vm.toggle(item)
                                  }
                                }
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "child_to_day_table_name_block",
                                    style: _vm.setPadding(item)
                                  },
                                  [
                                    _c("div", {
                                      staticClass: "h-100 px-3",
                                      class: _vm.iconName(item)
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "child_to_day_table_name_title",
                                        on: {
                                          dblclick: function($event) {
                                            ;(_vm.counter += 1),
                                              _vm.showTable(item)
                                          }
                                        }
                                      },
                                      [_vm._v(" " + _vm._s(item.name) + " ")]
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass:
                                  "child_to_day_border child_to_day_table_numbers",
                                attrs: { "data-th": "group_amount" }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(item.data[_vm.lastMonth].group) +
                                    " "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass:
                                  "child_to_day_border child_to_day_table_numbers",
                                attrs: { "data-th": "child_amount" }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      item.data[
                                        _vm.lastMonth
                                      ].child_amount_plan.toLocaleString()
                                    )
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass:
                                  "child_to_day_border child_to_day_table_numbers",
                                attrs: { "data-th": "child_amount" }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      item.data[
                                        _vm.lastMonth
                                      ].child_amount_fact.toLocaleString()
                                    )
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.months["numbers"], function(
                              monthNumber,
                              index
                            ) {
                              return _c(
                                "td",
                                {
                                  key: index,
                                  staticClass:
                                    "child_to_day_border child_to_day_table_numbers",
                                  class: _vm.classOfAttribute(item, index),
                                  attrs: { "data-th": monthNumber }
                                },
                                [
                                  _vm._v(
                                    "\n                                " +
                                      _vm._s(
                                        _vm.formatter(item, monthNumber, index)
                                      ) +
                                      "\n                            "
                                  )
                                ]
                              )
                            })
                          ],
                          2
                        )
                      }),
                      0
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.isShowTarify && _vm.currentPage == "budget",
                      expression: "isShowTarify && currentPage == 'budget'"
                    }
                  ],
                  staticClass: "modal_table",
                  attrs: { name: "kindergartentable" }
                },
                [
                  _c("div", { staticClass: "modal_container_table" }, [
                    _c(
                      "div",
                      {
                        staticClass: "child_to_day_head",
                        staticStyle: { "align-items": "flex-start" }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "child_to_day_head_formula" },
                          [
                            _c("div", [
                              _c("strong", [
                                _vm._v("Наименование организации:")
                              ]),
                              _vm._v(" " + _vm._s(_vm.selectedTitle) + " ")
                            ]),
                            _vm._v(" "),
                            _c("div", [
                              _c("strong", [_vm._v("Область:")]),
                              _vm._v(
                                " " +
                                  _vm._s(this.budgetData["oblast_name"]) +
                                  " "
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", [
                              _c("strong", [
                                _vm._v("Территориальная принадлежность: ")
                              ]),
                              _vm._v(
                                " " +
                                  _vm._s(
                                    this.budgetData["type_city"] == "selo"
                                      ? "село"
                                      : "город"
                                  ) +
                                  " "
                              )
                            ]),
                            _vm._v(" "),
                            this.budgetData["type_ecolog"] == "normal"
                              ? _c("div", [
                                  _c("strong", [_vm._v("Статус региона:")]),
                                  _vm._v(" Обычный регион  ")
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            this.budgetData["type_ecolog"] == "eco"
                              ? _c("div", [
                                  _c("strong", [_vm._v("Статус региона:")]),
                                  _vm._v(" В зоне радиации ")
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            this.budgetData["type_ecolog"] == "rad"
                              ? _c("div", [
                                  _c("strong", [_vm._v("Статус региона:")]),
                                  _vm._v(" В зоне экологии ")
                                ])
                              : _vm._e()
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "close",
                          staticStyle: { margin: "10px" },
                          on: { click: _vm.closeTarify }
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "child_to_day_head" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.kindergartenDate,
                            expression: "kindergartenDate"
                          }
                        ],
                        staticClass: "mx-3 px-2 py-1",
                        staticStyle: { "font-size": "14px", width: "200px" },
                        attrs: {
                          type: "month",
                          name: "date_start",
                          min: _vm.startDate,
                          max: _vm.currentDate,
                          required: ""
                        },
                        domProps: { value: _vm.kindergartenDate },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.kindergartenDate = $event.target.value
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm._m(0),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "mx-2",
                          staticStyle: { "font-size": "14px", color: "black" }
                        },
                        [
                          _c(
                            "button",
                            {
                              staticClass: "btn mx-3",
                              staticStyle: {
                                cursor: "pointer",
                                "font-size": "12px",
                                "background-color": "#5F46C5",
                                color: "white"
                              },
                              on: { click: _vm.exportTarifyToExcel }
                            },
                            [_vm._v(" Скачать в excel")]
                          )
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "scroll_style",
                        staticStyle: { "overflow-y": "scroll", height: "70%" }
                      },
                      [
                        _c(
                          "table",
                          {
                            ref: "childTarify",
                            staticClass: "table_tarify w-100"
                          },
                          [
                            _c("thead", [
                              _vm._m(1),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "5" } }, [
                                  _c("div", { staticClass: "hide_none" }, [
                                    _c("strong", [
                                      _vm._v("Наименование организации:")
                                    ]),
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedTitle) + " "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "5" } }, [
                                  _c("div", { staticClass: "hide_none" }, [
                                    _c("strong", [_vm._v("Область:")]),
                                    _vm._v(
                                      " " +
                                        _vm._s(this.budgetData["oblast_name"]) +
                                        " "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "5" } }, [
                                  _c("div", { staticClass: "hide_none" }, [
                                    _c("strong", [
                                      _vm._v("Территориальная принадлежность: ")
                                    ]),
                                    _vm._v(
                                      " " +
                                        _vm._s(
                                          this.budgetData["type_city"] == "selo"
                                            ? "село"
                                            : "город"
                                        ) +
                                        " "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "5" } }, [
                                  this.budgetData["type_ecolog"] == "normal"
                                    ? _c("div", { staticClass: "hide_none" }, [
                                        _c("strong", [
                                          _vm._v("Статус региона:")
                                        ]),
                                        _vm._v(" Обычный регион  ")
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  this.budgetData["type_ecolog"] == "eco"
                                    ? _c("div", { staticClass: "hide_none" }, [
                                        _c("strong", [
                                          _vm._v("Статус региона:")
                                        ]),
                                        _vm._v(" В зоне радиации ")
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  this.budgetData["type_ecolog"] == "rad"
                                    ? _c("div", { staticClass: "hide_none" }, [
                                        _c("strong", [
                                          _vm._v("Статус региона:")
                                        ]),
                                        _vm._v(" В зоне экологии ")
                                      ])
                                    : _vm._e()
                                ])
                              ]),
                              _vm._v(" "),
                              _vm._m(2),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "5" } }, [
                                  _c("div", { staticClass: "hide_none" }, [
                                    _vm._v(
                                      " Таблица расчета бюджета за " +
                                        _vm._s(_vm.kindergartenTableMonth) +
                                        " "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _vm._m(3),
                              _vm._v(" "),
                              _vm._m(4),
                              _vm._v(" "),
                              _vm._m(5),
                              _vm._v(" "),
                              _vm._m(6)
                            ]),
                            _vm._v(" "),
                            this.budgetData
                              ? _c(
                                  "tbody",
                                  [
                                    _vm._l(this.budgetData["groups"], function(
                                      budgetData,
                                      index
                                    ) {
                                      return _c("tr", { key: index }, [
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(budgetData["group"])
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData[
                                                      "tarify"
                                                    ].toLocaleString()
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData["child_amount"]
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData["working_days"]
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData[
                                                      "amount_plan"
                                                    ].toLocaleString()
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData[
                                                      "amount_fact"
                                                    ].toLocaleString()
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData[
                                                      "budget_plan"
                                                    ].toLocaleString()
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        budgetData
                                          ? _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    budgetData[
                                                      "budget_fact"
                                                    ].toLocaleString()
                                                  )
                                                )
                                              ]
                                            )
                                          : _vm._e()
                                      ])
                                    }),
                                    _vm._v(" "),
                                    _c("tr", [
                                      _c(
                                        "td",
                                        { staticClass: "child_to_day_border" },
                                        [_vm._v("Все группы")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "td",
                                        { staticClass: "child_to_day_border" },
                                        [_vm._v(" - ")]
                                      ),
                                      _vm._v(" "),
                                      this.budgetData
                                        ? _c(
                                            "td",
                                            {
                                              staticClass: "child_to_day_border"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  this.budgetData[
                                                    "child_amount"
                                                  ]
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      this.budgetData
                                        ? _c(
                                            "td",
                                            {
                                              staticClass: "child_to_day_border"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  this.budgetData[
                                                    "working_days"
                                                  ]
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      this.budgetData
                                        ? _c(
                                            "td",
                                            {
                                              staticClass: "child_to_day_border"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  this.budgetData[
                                                    "amount_plan"
                                                  ].toLocaleString()
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      this.budgetData
                                        ? _c(
                                            "td",
                                            {
                                              staticClass: "child_to_day_border"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  this.budgetData[
                                                    "amount_fact"
                                                  ].toLocaleString()
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      this.budgetData
                                        ? _c(
                                            "td",
                                            {
                                              staticClass: "child_to_day_border"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  this.budgetData[
                                                    "budget_plan"
                                                  ].toLocaleString()
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      this.budgetData
                                        ? _c(
                                            "td",
                                            {
                                              staticClass: "child_to_day_border"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  this.budgetData[
                                                    "budget_fact"
                                                  ].toLocaleString()
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e()
                                    ])
                                  ],
                                  2
                                )
                              : _vm._e()
                          ]
                        )
                      ]
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.isShowTable && _vm.currentPage == "amount",
                      expression: "isShowTable && currentPage == 'amount'"
                    }
                  ],
                  staticClass: "modal_table",
                  attrs: {
                    name: "kindergartentable",
                    width: "70%",
                    height: "auto"
                  }
                },
                [
                  _c("div", { staticClass: "modal_container_table" }, [
                    _c("div", { staticClass: "child_to_day_head" }, [
                      _c("div", { staticClass: "child_to_day_title" }, [
                        _vm._v(" " + _vm._s(_vm.selectedTitle) + " ")
                      ]),
                      _vm._v(" "),
                      _c("div", {
                        staticClass: "close",
                        on: { click: _vm.closeTable }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "kindergarten_filter",
                        staticStyle: { "justify-content": "space-between" }
                      },
                      [
                        _c("div", { staticClass: "kindergarten_filter" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.searchChild,
                                expression: "searchChild"
                              }
                            ],
                            staticClass: "search_table",
                            staticStyle: { "font-size": "14px", width: "30%" },
                            attrs: { type: "text", placeholder: "Поиск..." },
                            domProps: { value: _vm.searchChild },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.searchChild = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "stats_groups" }, [
                            _c(
                              "select",
                              {
                                attrs: { required: "" },
                                on: {
                                  "!change": function($event) {
                                    return _vm.selectGroup($event)
                                  }
                                }
                              },
                              [
                                _c("option", { domProps: { value: 0 } }, [
                                  _vm._v(" Все группы ")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.groups, function(group, k) {
                                  return _c(
                                    "option",
                                    { key: k, domProps: { value: group.id } },
                                    [_vm._v(" " + _vm._s(group.name) + " ")]
                                  )
                                })
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.kindergartenDate,
                                expression: "kindergartenDate"
                              }
                            ],
                            staticClass:
                              "mx-3 px-2 py-1 normal_text stats_month",
                            attrs: {
                              type: "month",
                              name: "date_start",
                              min: _vm.startDate,
                              max: _vm.currentDate,
                              required: ""
                            },
                            domProps: { value: _vm.kindergartenDate },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.kindergartenDate = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          !_vm.isMiddleScreen
                            ? _c(
                                "div",
                                { staticClass: "kindergarten_filter mx-3" },
                                [
                                  _c("div", {
                                    staticClass: "mx-2 missing_circle"
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticStyle: {
                                        "font-size": "12px",
                                        color: "black"
                                      }
                                    },
                                    [_vm._v("Пропуск")]
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.isMiddleScreen
                            ? _c(
                                "div",
                                { staticClass: "kindergarten_filter mr-3" },
                                [
                                  _c("div", {
                                    staticClass: "mx-2 wekeend_circle"
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticStyle: {
                                        "font-size": "12px",
                                        color: "black"
                                      }
                                    },
                                    [_vm._v("Отпуск")]
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.isMiddleScreen
                            ? _c(
                                "div",
                                { staticClass: "kindergarten_filter mr-3" },
                                [
                                  _c("div", {
                                    staticClass: "mx-2 sick_circle"
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticStyle: {
                                        "font-size": "12px",
                                        color: "black"
                                      }
                                    },
                                    [_vm._v("Больничный")]
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.isMiddleScreen
                            ? _c(
                                "div",
                                { staticClass: "kindergarten_filter mx-3" },
                                [
                                  _c("div", {
                                    staticClass: "mx-2 edited_missing_circle"
                                  }),
                                  _vm._v(" "),
                                  _vm._m(7)
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.isMiddleScreen
                            ? _c(
                                "div",
                                { staticClass: "kindergarten_filter mr-3" },
                                [
                                  _c("div", {
                                    staticClass: "mx-2 edited_wekeend_circle"
                                  }),
                                  _vm._v(" "),
                                  _vm._m(8)
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.isMiddleScreen
                            ? _c(
                                "div",
                                { staticClass: "kindergarten_filter mr-3" },
                                [
                                  _c("div", {
                                    staticClass: "mx-2 edited_sick_circle"
                                  }),
                                  _vm._v(" "),
                                  _vm._m(9)
                                ]
                              )
                            : _vm._e()
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "mx-2",
                            staticStyle: { "font-size": "14px", color: "black" }
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn mx-3",
                                staticStyle: {
                                  "font-size": "12px",
                                  "background-color": "#5F46C5",
                                  color: "white"
                                },
                                on: { click: _vm.exportTableToExcel }
                              },
                              [_vm._v(" Скачать в excel")]
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "scroll_style",
                        staticStyle: {
                          width: "100%",
                          height: "88%",
                          overflow: "scroll"
                        }
                      },
                      [
                        _c(
                          "table",
                          {
                            ref: "childTable",
                            staticClass: "table_child w-100"
                          },
                          [
                            _c("thead", [
                              _vm._m(10),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "10" } }, [
                                  _c("div", { staticClass: "hide_none" }, [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(_vm.selectedTitle) +
                                        "\n                                        "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _vm._m(11),
                              _vm._v(" "),
                              _vm._m(12),
                              _vm._v(" "),
                              _vm._m(13),
                              _vm._v(" "),
                              _c("tr", [
                                _c("th"),
                                _vm._v(" "),
                                _c("th", { attrs: { colspan: "10" } }, [
                                  _c("div", { staticClass: "hide_none" }, [
                                    _vm._v(
                                      "\n                                            Табель учета посещаемости детей за " +
                                        _vm._s(_vm.kindergartenTableMonth) +
                                        "\n                                        "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _vm._m(14),
                              _vm._v(" "),
                              _vm._m(15),
                              _vm._v(" "),
                              _c(
                                "tr",
                                { staticClass: "center" },
                                [
                                  _vm._m(16),
                                  _vm._v(" "),
                                  _vm._l(this.fetchedHeaders, function(
                                    header,
                                    index
                                  ) {
                                    return typeof header == "string"
                                      ? _c(
                                          "th",
                                          {
                                            key: index,
                                            staticClass: "child_to_day_header",
                                            staticStyle: { width: "5%" },
                                            style: _vm.tableHeaderStyle(header),
                                            attrs: { rowspan: "2" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                        " +
                                                _vm._s(header) +
                                                "\n                                    "
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass: "child_to_day_header",
                                      attrs: { colspan: _vm.amountDays }
                                    },
                                    [_vm._v(" Дни ")]
                                  ),
                                  _vm._v(" "),
                                  _vm._m(17)
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _c(
                                "tr",
                                _vm._l(this.fetchedHeaders, function(
                                  header,
                                  index
                                ) {
                                  return typeof header == "number"
                                    ? _c(
                                        "th",
                                        {
                                          key: index,
                                          staticClass: "child_to_day_header",
                                          staticStyle: { width: "3%" },
                                          style: _vm.tableHeaderStyle(header)
                                        },
                                        [
                                          _vm._v(
                                            "\n                                        " +
                                              _vm._s(header) +
                                              "\n                                    "
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                }),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              _vm._l(this.fetchedRows, function(row, index) {
                                return _c(
                                  "tr",
                                  {
                                    key: index,
                                    staticStyle: { height: "1px" }
                                  },
                                  [
                                    _c("td", [
                                      _c("div", { staticClass: "hide_none" }, [
                                        _vm._v(
                                          "\n                                            " +
                                            _vm._s(index + 1) +
                                            "\n                                        "
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { staticClass: "child_to_day_border" },
                                      [_vm._v(" " + _vm._s(row.name))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { staticClass: "child_to_day_border" },
                                      [
                                        _c("div", { staticClass: "cells" }, [
                                          _c(
                                            "div",
                                            { staticClass: "missing_day" },
                                            [_vm._v(_vm._s(row.missing))]
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { staticClass: "child_to_day_border " },
                                      [
                                        _c("div", { staticClass: "cells" }, [
                                          _c(
                                            "div",
                                            { staticClass: "sick_day" },
                                            [_vm._v(_vm._s(row.sick))]
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { staticClass: "child_to_day_border " },
                                      [
                                        _c("div", { staticClass: "cells" }, [
                                          _c(
                                            "div",
                                            { staticClass: "wekeend_day" },
                                            [_vm._v(_vm._s(row.wekeend))]
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { staticClass: "child_to_day_border" },
                                      [_vm._v(" " + _vm._s(row.group))]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.days, function(day, index) {
                                      return _c(
                                        "td",
                                        {
                                          key: index,
                                          staticClass: "child_to_day_border",
                                          class: _vm.dayClass(row[day])
                                        },
                                        [
                                          row[day].all == 1
                                            ? _c(
                                                "div",
                                                { staticClass: "cells" },
                                                [
                                                  _c("div", [
                                                    _vm._v(
                                                      _vm._s(
                                                        row[day].attendance
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            : _c(
                                                "div",
                                                { staticClass: "cells" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      class: _vm.attendanceClass(
                                                        row[day]
                                                      )
                                                    },
                                                    [
                                                      row[day].type_of_day == 2
                                                        ? _c("div", {}, [
                                                            row[day]
                                                              .type_of_day == 2
                                                              ? _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "hide_none"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      " В "
                                                                    )
                                                                  ]
                                                                )
                                                              : _vm._e()
                                                          ])
                                                        : _c("div", {}, [
                                                            row[day]
                                                              .attendance == 2
                                                              ? _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "hide_none"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      " ... "
                                                                    )
                                                                  ]
                                                                )
                                                              : row[day]
                                                                  .attendance ==
                                                                12
                                                              ? _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "hide_none"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      " ... "
                                                                    )
                                                                  ]
                                                                )
                                                              : _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "hide_none"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      " Н "
                                                                    )
                                                                  ]
                                                                )
                                                          ])
                                                    ]
                                                  )
                                                ]
                                              )
                                        ]
                                      )
                                    }),
                                    _vm._v(" "),
                                    _c("td", [
                                      row["reason"].attendance == 0
                                        ? _c(
                                            "div",
                                            { staticClass: "hide_none" },
                                            [_vm._v(" ... ")]
                                          )
                                        : _vm._e()
                                    ])
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ]
                        )
                      ]
                    )
                  ])
                ]
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.currentComponent == "graph" || _vm.isLargeScreen
          ? _c("div", { staticClass: "child_to_day_graphs" }, [
              _vm.currentPage == "amount"
                ? _c(
                    "div",
                    { staticClass: "child_to_day_graph mb-2" },
                    [
                      _vm._m(18),
                      _vm._v(" "),
                      _c("apexchart", {
                        attrs: {
                          type: "bar",
                          height: "80%",
                          options: _vm.chartsChildToDay.chartOptions,
                          series: _vm.chartsChildToDay.series
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "child_to_day_footer",
                          staticStyle: { height: "10%" }
                        },
                        [
                          _c("div", {
                            staticClass: "fullscreen",
                            on: {
                              click: function($event) {
                                return _vm.showModal("isChartsChildToDay")
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.isChartsChildToDay,
                              expression: "isChartsChildToDay"
                            }
                          ],
                          staticClass: "modal_graph",
                          attrs: {
                            name: "chartsChildToDay",
                            width: "70%",
                            height: "auto"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "modal_container" },
                            [
                              _c("div", { staticClass: "child_to_day_head" }, [
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_title" },
                                  [
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedTitle) + " "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_unit" },
                                  [_vm._v(" количество ")]
                                )
                              ]),
                              _vm._v(" "),
                              _c("apexchart", {
                                attrs: {
                                  type: "bar",
                                  height: "90%",
                                  options: _vm.chartsChildToDay.chartOptions,
                                  series: _vm.chartsChildToDay.series
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "child_to_day_footer" },
                                [
                                  _c("div", {
                                    staticClass: "shortscreen",
                                    on: {
                                      click: function($event) {
                                        return _vm.closeModal(
                                          "isChartsChildToDay"
                                        )
                                      }
                                    }
                                  })
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.currentPage == "amount"
                ? _c(
                    "div",
                    { staticClass: "child_to_day_graph" },
                    [
                      _vm._m(19),
                      _vm._v(" "),
                      _c("apexchart", {
                        attrs: {
                          type: "bar",
                          height: "80%",
                          options: _vm.chartsAttendanceChildToDay.chartOptions,
                          series: _vm.chartsAttendanceChildToDay.series
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "child_to_day_footer",
                          staticStyle: {
                            "justify-content": "space-between !important",
                            height: "10%"
                          }
                        },
                        [
                          _c("div", { staticClass: "kindergarten_filter" }, [
                            _c(
                              "div",
                              {
                                staticClass: "kindergarten_filter mx-3",
                                staticStyle: { width: "40%" }
                              },
                              [
                                _c("div", {
                                  staticClass: "mx-2 missing_circle"
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticStyle: {
                                      cursor: "pointer",
                                      "font-size": "14px",
                                      color: "black"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.showAttendance("missing")
                                      }
                                    }
                                  },
                                  [_vm._v("Пропуск")]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "kindergarten_filter mr-3",
                                staticStyle: { width: "40%" }
                              },
                              [
                                _c("div", {
                                  staticClass: "mx-2 wekeend_circle"
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticStyle: {
                                      cursor: "pointer",
                                      "font-size": "14px",
                                      color: "black"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.showAttendance("weekend")
                                      }
                                    }
                                  },
                                  [_vm._v("Отпуск")]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "kindergarten_filter mr-3",
                                staticStyle: { width: "40%" }
                              },
                              [
                                _c("div", { staticClass: "mx-2 sick_circle" }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticStyle: {
                                      cursor: "pointer",
                                      "font-size": "14px",
                                      color: "black"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.showAttendance("sick")
                                      }
                                    }
                                  },
                                  [_vm._v("Больничный")]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "fullscreen",
                            on: {
                              click: function($event) {
                                return _vm.showModal(
                                  "isChartsAttendanceChildToDay"
                                )
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value:
                                _vm.isKindergartenAttendance &&
                                this.amountOfTypeAttendance,
                              expression:
                                "isKindergartenAttendance && this.amountOfTypeAttendance"
                            }
                          ],
                          staticClass: "modal_graph",
                          attrs: {
                            name: "chartsAttendanceChildToDay",
                            width: "70%",
                            height: "auto"
                          }
                        },
                        [
                          _c("div", { staticClass: "modal_attendance" }, [
                            _c("div", { staticClass: "child_to_day_head" }, [
                              _c("div", { staticClass: "child_to_day_title" }, [
                                _vm._v(" " + _vm._s(_vm.selectedTitle) + " ")
                              ]),
                              _vm._v(" "),
                              _c("div", {
                                staticClass: "close",
                                on: {
                                  click: function($event) {
                                    return _vm.closeAttendance(
                                      "isKindergartenAttendance"
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticStyle: { height: "97%", overflow: "auto" }
                              },
                              [
                                _c(
                                  "table",
                                  { staticStyle: { width: "100%" } },
                                  [
                                    _c("thead", [
                                      _c("tr", [
                                        _c(
                                          "th",
                                          {
                                            staticClass: "child_to_day_header",
                                            staticStyle: {}
                                          },
                                          [_vm._v(" НАИМЕНОВАНИЕ")]
                                        ),
                                        _vm._v(" "),
                                        _vm.typeOfAttendance == "missing"
                                          ? _c(
                                              "th",
                                              {
                                                staticClass:
                                                  "child_to_day_header",
                                                staticStyle: {}
                                              },
                                              [_vm._v(" Кол-во Пропусков ")]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.typeOfAttendance == "sick"
                                          ? _c(
                                              "th",
                                              {
                                                staticClass:
                                                  "child_to_day_header",
                                                staticStyle: {}
                                              },
                                              [_vm._v(" Кол-во больничных ")]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.typeOfAttendance == "weekend"
                                          ? _c(
                                              "th",
                                              {
                                                staticClass:
                                                  "child_to_day_header",
                                                staticStyle: {}
                                              },
                                              [_vm._v(" Кол-во отпусков ")]
                                            )
                                          : _vm._e()
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "tbody",
                                      _vm._l(
                                        this.amountOfTypeAttendance,
                                        function(kindergarten, index) {
                                          return _c("tr", { key: index }, [
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border",
                                                staticStyle: {
                                                  "text-style": "left"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  " " +
                                                    _vm._s(kindergarten.name)
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "child_to_day_border",
                                                staticStyle: {}
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "cells" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        class:
                                                          _vm.classOfAttendance
                                                      },
                                                      [
                                                        _vm._v(
                                                          "\n                                                    " +
                                                            _vm._s(
                                                              kindergarten.sum
                                                            ) +
                                                            "\n                                                "
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ])
                                        }
                                      ),
                                      0
                                    )
                                  ]
                                )
                              ]
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.isChartsAttendanceChildToDay,
                              expression: "isChartsAttendanceChildToDay"
                            }
                          ],
                          staticClass: "modal_graph",
                          attrs: {
                            name: "chartsAttendanceChildToDay",
                            width: "70%",
                            height: "auto"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "modal_container" },
                            [
                              _c("div", { staticClass: "child_to_day_head" }, [
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_title" },
                                  [
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedTitle) + " "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_unit" },
                                  [_vm._v(" количество ")]
                                )
                              ]),
                              _vm._v(" "),
                              _c("apexchart", {
                                attrs: {
                                  type: "bar",
                                  height: "90%",
                                  options:
                                    _vm.chartsAttendanceChildToDay.chartOptions,
                                  series: _vm.chartsAttendanceChildToDay.series
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "child_to_day_footer" },
                                [
                                  _c("div", {
                                    staticClass: "shortscreen",
                                    on: {
                                      click: function($event) {
                                        return _vm.closeModal(
                                          "isChartsAttendanceChildToDay"
                                        )
                                      }
                                    }
                                  })
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.currentPage == "budget"
                ? _c(
                    "div",
                    { staticClass: "child_to_day_graph mb-2" },
                    [
                      _vm._m(20),
                      _vm._v(" "),
                      _c("apexchart", {
                        attrs: {
                          type: "bar",
                          height: "80%",
                          options: _vm.chartsBudgetChildToDay.chartOptions,
                          series: _vm.chartsBudgetChildToDay.series
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "child_to_day_footer",
                          staticStyle: { height: "10%" }
                        },
                        [
                          _c("div", {
                            staticClass: "fullscreen",
                            on: {
                              click: function($event) {
                                return _vm.showModal("isChartsBudgetChildToDay")
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.isChartsBudgetChildToDay,
                              expression: "isChartsBudgetChildToDay"
                            }
                          ],
                          staticClass: "modal_graph",
                          attrs: {
                            name: "chartsBudgetChildToDay",
                            width: "70%",
                            height: "auto"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "modal_container" },
                            [
                              _c("div", { staticClass: "child_to_day_head" }, [
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_title" },
                                  [
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedTitle) + " "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_unit" },
                                  [_vm._v(" количество ")]
                                )
                              ]),
                              _vm._v(" "),
                              _c("apexchart", {
                                attrs: {
                                  type: "bar",
                                  height: "90%",
                                  options:
                                    _vm.chartsBudgetChildToDay.chartOptions,
                                  series: _vm.chartsBudgetChildToDay.series
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "child_to_day_footer" },
                                [
                                  _c("div", {
                                    staticClass: "shortscreen",
                                    on: {
                                      click: function($event) {
                                        return _vm.closeModal(
                                          "isChartsBudgetChildToDay"
                                        )
                                      }
                                    }
                                  })
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.currentPage == "budget"
                ? _c(
                    "div",
                    { staticClass: "child_to_day_graph" },
                    [
                      _vm._m(21),
                      _vm._v(" "),
                      _c("apexchart", {
                        attrs: {
                          type: "bar",
                          height: "80%",
                          options:
                            _vm.chartsBudgetDifferenceChildToDay.chartOptions,
                          series: _vm.chartsBudgetDifferenceChildToDay.series
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "child_to_day_footer",
                          staticStyle: { height: "10%" }
                        },
                        [
                          _c("div", {
                            staticClass: "fullscreen",
                            on: {
                              click: function($event) {
                                return _vm.showModal(
                                  "isChartsBudgetDifferenceChildToDay"
                                )
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.isChartsBudgetDifferenceChildToDay,
                              expression: "isChartsBudgetDifferenceChildToDay"
                            }
                          ],
                          staticClass: "modal_graph",
                          attrs: {
                            name: "chartsBudgetDifferenceChildToDay",
                            width: "70%",
                            height: "auto"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "modal_container" },
                            [
                              _c("div", { staticClass: "child_to_day_head" }, [
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_title" },
                                  [
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedTitle) + " "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "child_to_day_unit" },
                                  [_vm._v(" количество ")]
                                )
                              ]),
                              _vm._v(" "),
                              _c("apexchart", {
                                attrs: {
                                  type: "bar",
                                  height: "90%",
                                  options:
                                    _vm.chartsBudgetDifferenceChildToDay
                                      .chartOptions,
                                  series:
                                    _vm.chartsBudgetDifferenceChildToDay.series
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "child_to_day_footer" },
                                [
                                  _c("div", {
                                    staticClass: "shortscreen",
                                    on: {
                                      click: function($event) {
                                        return _vm.closeModal(
                                          "isChartsBudgetDifferenceChildToDay"
                                        )
                                      }
                                    }
                                  })
                                ]
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                : _vm._e()
            ])
          : _vm._e()
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "kindergarten_filter" }, [
      _c("div", { staticClass: "child_to_day_title" }, [
        _vm._v(
          " ФОРМУЛА: БЮДЖЕТ = ТАРИФ / КОЛ-ВО РАБ. ДНЕЙ * КОЛ-ВО ПОСЕЩЕНИИ "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("th"),
      _vm._v(" "),
      _c("th", { attrs: { colspan: "5" } }, [
        _c("div", { staticClass: "hide_none" }, [
          _vm._v(
            " ФОРМУЛА: БЮДЖЕТ = ТАРИФ / КОЛ-ВО РАБ. ДНЕЙ * КОЛ-ВО ПОСЕЩЕНИИ "
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", { staticClass: "center" }, [
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Группа")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Тариф ")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Кол-во детей ")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Кол-во раб. дней")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Проектная мощность")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Фактическая кол-во")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Плановый бюджет ")]
      ),
      _vm._v(" "),
      _c(
        "th",
        {
          staticClass: "child_to_day_header",
          staticStyle: { padding: "15px" }
        },
        [_vm._v(" Фактический бюджет ")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticStyle: { "font-size": "12px", color: "black" } }, [
      _c("span", [_vm._v(" Отред.пропуск ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticStyle: { "font-size": "12px", color: "black" } }, [
      _c("span", [_vm._v(" Отред.отпуск ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticStyle: { "font-size": "12px", color: "black" } }, [
      _c("span", [_vm._v(" Отред.больничный ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("th"),
      _vm._v(" "),
      _c("th", { attrs: { colspan: "10" } }, [
        _c("div", { staticClass: "hide_none" }, [
          _vm._v(
            "\n                                            Наименование государственного учреждения (централизованной бухгалтерии)\n                                        "
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("th")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", { staticStyle: { display: "table-column" } }, [
      _c("th"),
      _vm._v(" "),
      _c("th"),
      _vm._v(" "),
      _c("th"),
      _vm._v(" "),
      _c("th"),
      _vm._v(" "),
      _c("th"),
      _vm._v(" "),
      _c("th"),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")]),
      _vm._v(" "),
      _c("th", [_vm._v("..")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("th", { attrs: { rowspan: "2" } }, [
      _c("div", { staticClass: "hide_none" }, [_vm._v(" № п/п ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("th", { attrs: { rowspan: "2" } }, [
      _c("div", { staticClass: "hide_none" }, [
        _vm._v(
          "\n                                            Причины  непосещения  (основание)\n                                        "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "child_to_day_head", staticStyle: { height: "10%" } },
      [
        _c("div", { staticClass: "child_to_day_title" }, [
          _vm._v(" План/Факт отметок ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "child_to_day_unit" }, [
          _vm._v(" количество ")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "child_to_day_head", staticStyle: { height: "10%" } },
      [
        _c("div", { staticClass: "child_to_day_title" }, [
          _vm._v(" Отсутсвующие ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "child_to_day_unit" }, [
          _vm._v(" количество ")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "child_to_day_head", staticStyle: { height: "10%" } },
      [
        _c("div", { staticClass: "child_to_day_title" }, [
          _vm._v(" План/Факт Бюджет ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "child_to_day_unit" }, [
          _vm._v(" тыс. тенге ")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "child_to_day_head", staticStyle: { height: "10%" } },
      [
        _c("div", { staticClass: "child_to_day_title" }, [
          _vm._v(" Экономия бюджета ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "child_to_day_unit" }, [
          _vm._v(" тыс. тенге ")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/chart_options_attendance_child_to_day.json":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/dashboard/chart_options_attendance_child_to_day.json ***!
  \*********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"chartOptions":{"title":{"text":"Отсутсвующие","align":"center","margin":15,"style":{"fontSize":"14px","fontWeight":600,"fontFamily":"Nunito","color":"#263238"}},"grid":{"padding":{"right":30,"left":30}},"fill":{"opacity":1},"dataLabels":{"enabled":true},"legend":{"show":true,"position":"bottom"},"chart":{"height":"100%","stacked":true,"type":"bar"},"responsive":[{"options":{"legend":{"position":"bottom"}}}],"plotOptions":{"bar":{"horizontal":false,"dataLabels":{"total":{"enabled":true,"style":{"fontSize":"12px","fontWeight":600}}}}},"xaxis":{"categories":[],"tickPlacement":"on"},"yaxis":{"title":{"text":"количетсво"}}}}');

/***/ }),

/***/ "./resources/js/views/dashboard/chart_options_budget_child_to_day.json":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/dashboard/chart_options_budget_child_to_day.json ***!
  \*****************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"chartOptions":{"title":{"text":"План/Факт Бюджет","align":"center","margin":15,"style":{"fontSize":"14px","fontWeight":600,"fontFamily":"Nunito","color":"#263238"}},"grid":{},"fill":{"opacity":1},"dataLabels":{"enabled":true},"legend":{"show":true,"position":"bottom"},"responsive":[{"options":{"legend":{"position":"bottom"}}}],"chart":{"height":"100%","type":"bar","stacked":false,"toolbar":{"show":true},"zoom":{"enabled":true}},"xaxis":{"categories":[]},"yaxis":{"title":{"text":"тыс. тенге"}}}}');

/***/ }),

/***/ "./resources/js/views/dashboard/chart_options_budget_difference_child_to_day.json":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/chart_options_budget_difference_child_to_day.json ***!
  \****************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"chartOptions":{"title":{"text":"Экономия бюджета","align":"center","margin":15,"style":{"fontSize":"14px","fontWeight":600,"fontFamily":"Nunito","color":"#263238"}},"grid":{},"fill":{"opacity":1},"dataLabels":{"enabled":true},"legend":{"show":true,"position":"bottom"},"responsive":[{"options":{"legend":{"position":"bottom"}}}],"chart":{"height":"100%","type":"bar","toolbar":{"show":true},"zoom":{"enabled":true}},"xaxis":{"categories":[]},"yaxis":{"title":{"text":"тыс. тенге"}}}}');

/***/ }),

/***/ "./resources/js/views/dashboard/chart_options_child_to_day.json":
/*!**********************************************************************!*\
  !*** ./resources/js/views/dashboard/chart_options_child_to_day.json ***!
  \**********************************************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"chartOptions":{"title":{"text":"План/Факт отметок","align":"center","margin":15,"style":{"fontSize":"14px","fontWeight":600,"fontFamily":"Nunito","color":"#263238"}},"grid":{},"fill":{"opacity":1},"dataLabels":{"color":"black","enabled":true},"legend":{"show":true,"position":"bottom"},"responsive":[{"options":{"legend":{"position":"bottom"}}}],"chart":{"height":"100%","type":"bar","toolbar":{"show":true,"autoSelected":"zoom"},"zoom":{"enabled":true}},"xaxis":{"categories":[]},"yaxis":{"title":{"text":"количетсво","style":{"fontSize":"12px","fontWeight":600,"fontFamily":"Nunito","color":"#263238"}}}}}');

/***/ }),

/***/ "./resources/js/views/dashboard/chart_options_reason_of_missing.json":
/*!***************************************************************************!*\
  !*** ./resources/js/views/dashboard/chart_options_reason_of_missing.json ***!
  \***************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"chartOptions":{"chart":{"type":"donut"},"labels":["Пропуск","Больничный","Отпуск"],"legend":{"show":false},"responsive":[{"breakpoint":480,"options":{"chart":{"width":"100%"},"legend":{"show":false}}}]}}');

/***/ })

}]);