<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Teachers;

class Groups extends Model
{
    use HasFactory;

    protected $table = "groups";

    protected $fillable = ["name", "id_org", "count_place", "type_group", "age"];

    public function teacher()
    {
        return $this->hasOne(Teachers::class,'id_group', 'id')->withDefault();
    }
}