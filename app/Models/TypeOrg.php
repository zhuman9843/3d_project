<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOrg extends Model
{
    use HasFactory;

    protected $table = "type_org";

    protected $fillable = ["type_org", "type_org_code"];
}
