<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegionGroup extends Model
{
    use HasFactory;

    protected $table = "region_group";

    protected $fillable = ["group_type"];
}
