<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Childs;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Organizations;
use App\Models\Managers;
use App\Models\Regions;
use App\Models\Districts;
use App\Models\FilteredOrganizations;
use App\Models\TypeOrg;
use App\Models\TypeLocation;
use App\Models\TypeRegions;
use App\Models\TypeProject;
use App\Models\TableAnalyticUsers;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    private $childs;

    public function __construct()
    {   
        $this->childs = Childs::all();
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {

            $userEmail = Auth::user()->email;
            $userRole = Auth::user()->role;
            
            if( $userRole == 0 ) {
                $isBlocked = FilteredOrganizations::where('bin', $userEmail)->pluck('is_blocked')->first();
                
                if ($isBlocked == 1) {
                    return response()->json(['error' => 'Ваша организация заблокированa, обратитесь в службу поддержки!'], 401);
                }

                $organization = Organizations::where('bin', $userEmail)->first();
                
                $fact_amount = $this->childs->where('id_org', $organization->id)->count();
                $organization->fact_amount = ($fact_amount) ? $fact_amount : 0;
                $organization->type_project = TypeProject::where('id', $organization->type_project)->pluck('project')->first();
                $organization->region = Regions::where('id', $organization->region)->pluck('region')->first();
                $organization->district = Districts::where('id', $organization->district)->pluck('district')->first();
                $organization->type_org = TypeOrg::where('id', $organization->type_org)->pluck('type_org')->first();
                $organization->type_location = TypeLocation::where('id', $organization->type_location)->pluck('type_location')->first();
                $organization->type_region = TypeRegions::where('id', $organization->type_region)->pluck('type_region')->first();
                $organization->role = $userRole;
                $organization->save;
                $userInfo = $organization;
            } else if ( $userRole == 2 ) {
                $user = TableAnalyticUsers::where('email', $userEmail)->first();
                $user->role = $userRole;
                $user->save;
                $userInfo = $user;
            } else if ( $userRole == 10 ) {
                $manager = Managers::where('email', $userEmail)->first();
                $manager->role = $userRole;
                $manager->save;
                $userInfo = $manager;
            }

            return response()->json(['user' => $userInfo, 'token' =>  $token], 200)->header('Authorization', $token);
        }
        return response()->json(['error' => 'Неправильное имя пользователя или пароль'], 401);
    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );
        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'message' => 'Password reset email sent.',
            'data' => $response
        ]);
    }

    protected function credentials(Request $request)
    {
        return $request->only('email');
    }

    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
    }

    public function callResetPassword(Request $request)
    {
        return $this->reset($request);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Email could not be sent to this email address.']);
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->save();
        event(new PasswordReset($user));
    }

    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    private function guard()
    {
        return Auth::guard();
    }

    public function broker()
    {
        return Password::broker();
    }
}