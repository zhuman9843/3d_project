import axios from "axios";

let token = localStorage.getItem('token');
axios.defaults.baseURL = "/"; 
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

export default axios;
