<?php

namespace Database\Seeders;

use App\Models\TypeOfAttendances;
use Illuminate\Database\Seeder;


class TypeOfAttendancesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $typeOfAttendances = [
        [
            'attendance' => 'Присутствует',
            'index_of_attendance' => '2',
            'coef_of_attendance' => '1',
        ],
        [
            'attendance' => 'Болеет',
            'index_of_attendance' => '4',
            'coef_of_attendance' => '1',
        ],
        [
            'attendance' => 'В отпуске',
            'index_of_attendance' => '5',
            'coef_of_attendance' => '1',
        ],
        [
            'attendance' => 'Отсутствует',
            'index_of_attendance' => '6',
            'coef_of_attendance' => '0',
        ],
        [
            'attendance' => 'Не числится',
            'index_of_attendance' => '7',
            'coef_of_attendance' => '0',
        ],
        [
            'attendance' => 'Ложное фото',
            'index_of_attendance' => '10',
            'coef_of_attendance' => '0',
        ],
    ];

    public function run()
    {
        foreach ( $this->typeOfAttendances as $typeOfAttendance) {
            $attendance = new TypeOfAttendances();
            $attendance->attendance          = $typeOfAttendance['attendance'];
            $attendance->index_of_attendance = $typeOfAttendance['index_of_attendance'];
            $attendance->coef_of_attendance  = $typeOfAttendance['coef_of_attendance'];
            $attendance->save();
        }
    }
}
