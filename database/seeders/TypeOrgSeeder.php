<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOrgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_org')->insert([
            'type_org' => 'Частный',
            'type_org_code' => 'private',
        ]);
        DB::table('type_org')->insert([
            'type_org' => 'Государственный',
            'type_org_code' => 'gos',
        ]);
    }
}
