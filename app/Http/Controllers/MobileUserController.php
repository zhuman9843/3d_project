<?php

namespace App\Http\Controllers;

use App\Models\Employees;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Contracts\Providers\Auth;
use App\Models\User;
use App\Models\Teachers;
use App\Models\TypeProject;
use App\Models\Organizations;
use App\Models\EmployeesToDay;
use Carbon\Carbon;

class MobileUserController extends Controller
{
    private $teachers;
    private $employees;
    private $organizations;

    public function __construct()
    {
        $this->teachers = Teachers::all();
        $this->employees = Employees::all();
        $this->organizations = Organizations::all();
    }

    public function login(Request $request) {
        $credentials = $request->only('email', 'password');

        $role = $request->role;

        if ($role == 1) {
            $teacherInfo = $this->teachers->where('iin', $request->input('email'));
            $idOrganization = $request->id_org;
            $teacherName = $teacherInfo->pluck('name')->first();
            $teacherId = $teacherInfo->pluck('id')->first();
            $email = $teacherInfo->pluck('email')->first();
            $telephone = $teacherInfo->pluck('telephone')->first();
            $organizations = Organizations::where('id', $idOrganization);
            $organizationName = $organizations->pluck('name')->first();
            $typeProjectId = $organizations->pluck('type_project')->first();
            $typeProject = TypeProject::where('id', $typeProjectId)->pluck('project')->first();
            $location = $organizations->pluck('location')->first();
            list($latitude, $longitude) = explode(', ', $location);
            
            try {
                if(!$token = JWTAuth::attempt($credentials)) {
                    return response()->json("Failed to create token", 500);
                }
                
                $result = [
                    'token' => $token,
                    'teacher_id' => $teacherId,
                    'teacher_name' => $teacherName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'location' => [
                        "latitude" => $latitude,
                        "longitude" => $longitude,
                    ],
                    'organization_name' => $organizationName,
                    'type_project' => $typeProject
                ];
                return response()->json($result);
            }catch(JWTException $e) {
                return response()->json($e->getMessage());
            }
        } else if ( $role == 3 ) {

            $employeeInfo = $this->employees->where('iin', $request->input('email'));
            $idOrganization = $employeeInfo->pluck('id_org')->first();
            $employeeName = $employeeInfo->pluck('name')->first();
            $employeeId = $employeeInfo->pluck('id')->first();
            $email = $employeeInfo->pluck('email')->first();
            $telephone = $employeeInfo->pluck('telephone')->first();
            $organizations = Organizations::where('id', $idOrganization);
            $organizationName = $organizations->pluck('name')->first();
            $typeProjectId = $organizations->pluck('type_project')->first();
            $typeProject = TypeProject::where('id', $typeProjectId)->pluck('project')->first();
            $location = $organizations->pluck('location')->first();
            list($latitude, $longitude) = explode(', ', $location);

            try {
                if(!$token = JWTAuth::attempt($credentials)) {
                    return response()->json("Failed to create token", 500);
                }

                $result = [
                    'token' => $token,
                    'teacher_id' => $employeeId,
                    'teacher_name' => $employeeName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'location' => [
                        "latitude" => $latitude,
                        "longitude" => $longitude,
                    ],
                    'organization_name' => $organizationName,
                    'type_project' => $typeProject
                ];
                return response()->json($result);
            }catch(JWTException $e) {
                return response()->json($e->getMessage());
            }
        } else if ( $role == 0) {
            $idOrganization = $request->id_org;
            $organizations = Organizations::where('id', $idOrganization);
            $organizationName = $organizations->pluck('name')->first();
            $email = $organizations->pluck('email')->first();
            $telephone = $organizations->pluck('telephone')->first();
            $typeProjectId = $organizations->pluck('type_project')->first();
            $typeProject = TypeProject::where('id', $typeProjectId)->pluck('project')->first();
            $location = $organizations->pluck('location')->first();
            list($latitude, $longitude) = explode(', ', $location);

            try {
                if(!$token = JWTAuth::attempt($credentials)) {
                    return response()->json("Failed to create token", 500);
                }

                $result = [
                    'token' => $token,
                    'id_org' => $idOrganization,
                    'role' => $role,
                    'email' => $email,
                    'telephone' => $telephone,
                    'location' => [
                        "latitude" => $latitude,
                        "longitude" => $longitude,
                    ],
                    'organization_name' => $organizationName,
                    'type_project' => $typeProject
                ];
                
                return response()->json($result);
            }catch(JWTException $e) {
                return response()->json($e->getMessage());
            }
        }
    }

    public function registerTeacher(Request $request)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $request->validate([
            'name' => 'required|string|max:255',
            'iin' => 'required|numeric|min:12',
            'telephone' => 'required|numeric|min:11',
            'email' => 'required|email|max:255',
            'birth_date' => 'required|date_format:Y-m-d',
            'password'  => 'required|min:3|confirmed',
        ]);

        $time = Carbon::now();
        $name = mb_strtolower($request->name, 'UTF-8');
        $name = mb_convert_case($name, MB_CASE_TITLE, 'UTF-8');

        $checkExistingTeacher = Teachers::where('iin', $request->iin)->where('id_org', $orgId)->first();

        if ($checkExistingTeacher) {
            return response()->json(['errors' => 'ИИН уже зарегистрирован!'], 500);
        }
        
        $teacher = new Teachers();
        $teacher->name = $name;
        $teacher->iin = $request->iin;
        $teacher->id_org = $orgId;
        $teacher->id_group = "[]";
        $teacher->telephone = $request->telephone;
        $teacher->birth_date = $request->birth_date;
        $teacher->email = $request->email;
        $teacher->created_at = $time;
        $teacher->updated_at = $time;
        
        $user = new User();
        $user->email = $request->iin;
        $user->password = bcrypt($request->password);
        $user->role = '1';
        $user->created_at = $time;
        $user->updated_at = $time;
        $user->save();
        $teacher->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function registerEmployee(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        
        $request->validate([
            'name' => 'required|string|max:255',
            'iin' => 'required|numeric|min:12',
            'telephone' => 'required|numeric|min:11',
            'job' => 'required|string',
            'email' => 'required|email|max:255',
            'birth_date' => 'required|date_format:Y-m-d',
            'password'  => 'required|min:3|confirmed',
        ]);

        $time = Carbon::now();
        $name = mb_strtolower($request->name, 'UTF-8');
        $name = mb_convert_case($name, MB_CASE_TITLE, 'UTF-8');

        $checkExistingEmployee = Employees::where('iin', $request->iin)->first();

        if ($checkExistingEmployee) {
            return response()->json(['errors' => 'ИИН уже зарегистрирован!'], 500);
        }

        $employee = new Employees();
        $employee->name = $name;
        $employee->iin = $request->iin;
        $employee->id_org = $orgId;
        $employee->job = $request->job;
        $employee->telephone = $request->telephone;
        $employee->birth_date = $request->birth_date;
        $employee->email = $request->email;
        $employee->created_at = $time;
        $employee->updated_at = $time;
        
        $user = new User();
        $user->email = $request->iin;
        $user->password = bcrypt($request->password);
        $user->role = '3';
        $user->created_at = $time;
        $user->updated_at = $time;
        $user->save();
        
        $employee->save();
        
        $employeesToDay = new EmployeesToDay([
            'id_employee' => $employee->id,
            'job' => $request->job,
            'id_org' => $orgId,
            'month' => Carbon::now()->format('Y-m')."-01"
        ]);
        $employeesToDay->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function emailByProject(Request $request) {
        $email = $request->email;

        $teachersOrgs = Teachers::where('iin', $email)->pluck('id_org');
        $employeesOrg = Employees::where('iin', $email)->pluck('job', 'id_org')->toArray();
        $org = Organizations::where('bin', $email)->get(['id', 'name'])->first();

        $organizationsTeacher = Organizations::whereIn('id', $teachersOrgs)->get(['id', 'type_project', 'name'])->map(function($org) {
            $job =  ( $org->type_project == 1 ) ? 'Воспитатель' : 'Куратор';

            $org = [
                'job' => $job,
                'role' => 1,
                'id_org' => $org->id,
                'organization' => $org->name,
            ];
            return $org;
        })->toArray();

        if ($org) {
            $organization = [
                'job' => '',
                'role' => 0,
                'id_org' => $org->id,
                'organization' => $org->name
            ];
            return [$organization];
        }
        if ($employeesOrg) {
            $organizationsEmployees = Organizations::where('id', array_keys($employeesOrg)[0])->get(['id', 'name'])->first();

            if ($organizationsEmployees) {
                $job = array_values($employeesOrg)[0];
                $organizations = array_merge($organizationsTeacher, [[ 
                    'job' => $job,
                    'role' => 3,
                    'id_org' => $organizationsEmployees->id,
                    'organization' => $organizationsEmployees->name
                ]]);
                return $organizations;
            } 
        } else {
            return $organizationsTeacher;
        }

    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    private function guard()
    {
        return Auth::guard();
    }
}
