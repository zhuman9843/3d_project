<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ResetRequest;
use App\Http\Requests\ForgotRequest;
use App\Models\Organizations;
use Illuminate\Mail\Message;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ForgotController extends Controller
{

    public function forgot(ForgotRequest $request)
    {
        $email = $request->input('email');

        if (User::where('email', $email)->doesntExist()) {
            return response([
                'message' => 'Пользователь не существует.'
            ], 404);
        }

        $token = Str::random(10);

        try {
            DB::table('password_resets')->insert([
                'email' => $email,
                'token' => $token
            ]);

            Mail::send('Mails.forgot', ['token' => $token], function(Message $message) use ($email){
                $message->to($email);
                $message->subject('Reset your password');
            });

            return response([
                'message' => 'Проверьте свою электронную почту!'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function resetFromEmail(Request $request){
        $token = $request->input('token');

        if (!$passwordResets = DB::Table('password_resets')->where('token', $token)->first()) {
            return response([
                'message' => 'Неверный токен!'
            ], 400);
        }

        if (!$organization = Organizations::where('email', $passwordResets->email)->first()) {
            return response([
                'message' => 'Пользователь не существует!'
            ], 404);
        }
        
        $user = User::where('email', $organization->bin)->first();
        $user->password = Hash::make($request->input('password'));

        $user->save();

        return response([
            'message' => 'Success'
        ]);
    }
    
    public function reset(ResetRequest $request){
        $credentials = $request->only('email', 'password');
        $token = $request->token;

        try {
            if($token == JWTAuth::attempt($credentials)) {
                return response()->json("Invalid password!", 500);
            }
        } catch(JWTException $e) {
            return response()->json($e->getMessage());
        }

        if (!$user = User::where('email', $request->email)->first()) {
            return response([
                'message' => 'User doesn\'t exist!'
            ], 404);
        }

        $user->password = Hash::make($request->input('password'));

        $user->save();

        return response([
            'message' => 'Success'
        ]);
    }
}
