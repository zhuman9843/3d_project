<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailNotification;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index() 
    {
        return view('home.index');
    }

    public function sendErrorToMail(Request $request) {
        $recipient = $request->input('recipient');
        $message = $request->input('message');

        Mail::to($recipient)->send(new EmailNotification($message));
    }
    
    public function sendErrorToDB(Request $request) {

        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $orgId = Organizations::where('email', $user->email)->pluck('id')->first();
        $recipient = $request->input('recipient');
        $message = $request->input('message');

        Mail::to($recipient)->send(new EmailNotification($message));
    }
    
    public function uploadPdf(Request $request)
    {
        $request->validate([
            'pdf_file' => 'required|file|max:16384'
        ]);

        $pdfFile = $request->file('pdf_file');
        $path = $pdfFile->store('temp');

        return response()->json(['path' => $path]);
    }

    public function extractPdfData(Request $request)
    {
        $pdfFilePath = storage_path('app/' . $request->input('path'));
        $csv = [];
        
        $text = shell_exec("pdftotext \"$pdfFilePath\" -");

        $csv = $this->getIik($csv, $text);
        $csv = $this->getReference($csv, $text);
        $csv = $this->getDate($csv, $text);
        $csv = $this->getDocNumber($csv, $text);
        $csv = $this->getPaymentType($csv, $text);
        $csv = $this->getTotalSum($csv, $text);

        $employees = $this->getTableText($text);
        
        foreach($employees as &$employee) {
            $employee = $csv + $employee;
        }

        return [
            'data' => $employees,
            'reference' => $csv['Референс']
        ];
    }

    private function getTableText($text) {
        $tableTextPattern = '/ФИО\s*([\s\S]+?)(?=(?:Платежное поручение|Референс:|$))/u';

        if (preg_match($tableTextPattern, $text, $matches)) {
            $purpose = trim($matches[1]);
            $purpose = str_replace("ИИН", "", $purpose);
            $purpose = str_replace("Дата рожд.", "", $purpose);
            $purpose = str_replace("Сумма", "", $purpose);
            $purpose = str_replace("Тип", "", $purpose);
            $purpose = str_replace("Период", "", $purpose);
            $purpose = str_replace(",00", "", $purpose);
            $purpose = preg_replace('/\n+/', ' ', $purpose);
        } else {
            echo "Purpose not found."; 
        }
        
        $modifiedPurpose = preg_replace('/(?<=\D)(\d{2}|\d)(?=\s)/', "\n$1", $purpose);
        
        $lines = explode("\n", $modifiedPurpose);

        $result = [];

        foreach ($lines as $line) {
            $line = trim($line);

            if ($line === '') {
                continue;
            }

            $parts = preg_split('/\s+/', $line, -1, PREG_SPLIT_NO_EMPTY);

            if (count($parts) >= 4) {
                $number = $parts[0];
                $name = implode(' ', array_slice($parts, 1, -2));
                $sum = $parts[count($parts) - 2];
                $iin = $parts[count($parts) - 1];

                $result[] = [
                    'ФИО' => $name,
                    'ИИН' => $iin,
                    'Лицевой счет получателя' => 1,
                    'Сумма' => $sum,
                ];
            }
        }

        return $result;
    }

    private function getIik($csv, $text) {
        $iik = '/KZ\d+/u';

        if (preg_match($iik, $text, $matches)) {
            $csv["ИИК"] = $matches[0]; 
        } else {
            echo "No 'KZ' value found.";
        }
        return $csv;
    }
    
    private function getReference($csv, $text) {
        $reference = '/Референс:\s*(\w+)/u';

        if (preg_match($reference, $text, $matches)) {
            $csv["Вид платежа"] = 1;
            $csv["Источник финансирования"] = 1;
            $csv["Референс"] = $matches[1];
        } else {
            echo "Reference not found.";
        }

        return $csv;
    }
    
    private function getDate($csv, $text) {
        $date = '/____(\d{2}-\d{2}-\d{4})____/u';

        if (preg_match($date, $text, $matches)) {
            $modifiedDate = str_replace("-", ".", $matches[1]);
            $csv["Дата"] = $modifiedDate;
        } else {
            echo "Date not found.";
        }

        return $csv;
    }
    
    private function getDocNumber($csv, $text) {
        $docNumber = '/Платежное поручение № (\d+)/u';

        if (preg_match($docNumber, $text, $matches)) {
            $csv["№ документа"] = $matches[1];
            $csv["ЕБК"] = 1;
            $csv["Специфика"] = 1;
        } else {
            echo "Document number not found.";
        }

        return $csv;
    }
    
    private function getPaymentType($csv, $text) {
        $pattern = '/Назначение платежа:(.*?)(Басшы \/ Руководитель)/s';

        if (preg_match($pattern, $text, $matches)) {
            $purpose = trim($matches[1]); 
            $pattern2 = '/\d{2}-\d{2}-\d{4}|/s';
            $purpose = preg_replace($pattern2, '', $purpose);
            $purpose = preg_replace('/\n+/', ' ', $purpose);
            $purpose = str_replace("Валюталау күні / Дата валютирования", "", $purpose);
        
            $csv["Назначение платежа"] = $purpose;
        } else {
            echo "Purpose not found.";
        }

        return $csv;
    }

    private function getTotalSum($csv, $text) {
        $pattern = '/Назначение платежа:(.*?)Дата валютирования/s'; // Regular expression pattern

        if (preg_match($pattern, $text, $matches)) {
            $purposeAndDate = trim($matches[1]); // Extract and trim the purpose and date text
            $pattern2 = '/([\d-]+)/';
            if (preg_match_all($pattern2, $purposeAndDate, $numbers)) {
                $sum = str_replace("-00", "", $numbers[0]);
                $csv["Сумма по счету"] = implode(' ', $sum);
            } else {
                echo "Numbers not found after 'Назначение платежа:' and before 'Дата валютирования'.";
            }
        } else {
            echo "Text not found between 'Назначение платежа:' and 'Дата валютирования'.";
        }

        return $csv;
    }
}
