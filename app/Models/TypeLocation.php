<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeLocation extends Model
{
    use HasFactory;

    protected $table = "type_location";

    protected $fillable = ["type_location", "type_location_code"];
}
