<?php

namespace App\Imports;

use App\Models\Groups;
use App\Models\GroupsAgeRanges;
use App\Models\TypeOfGroup;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class GroupsImport implements ToModel, WithStartRow
{
    const START_ROW = 2;
    
    protected $orgId;
    protected $typeGroups;
    protected $groupsAge;
    protected $groupNames;
    public $groupsImported;

    public function __construct($orgId)
    {
        $this->orgId = $orgId;
        $this->typeGroups = TypeOfGroup::all();
        $this->groupsAge = GroupsAgeRanges::all();
        $this->groupNames = Groups::where('id_org', $orgId)->pluck('name')->toArray();
        $this->groupsImported = [];
    }

    public function model(array $row)
    {
        if ($row[0] != "") {

            preg_match('/"(.*?)"/', $row[0], $matches);
            
            if ( $matches == []) {
                $name = $row[0];
            } else {
                $name = isset($matches[1]) ? $matches[1] : '';
            }
            
            $name = str_replace(' ', '', $name);
            
            if (in_array($name, $this->groupNames)) {
                return "Дублирование имени группы";
            }

            if (!in_array($name, $this->groupsImported)) {

                if ($row[2]) {
                    try {
                        $typeGroup = $this->typeGroups->where('type_of_group', $row[2])->pluck('category')->first();
                    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                        return "В одной из групп неправильно заполнен режим";
                        return $e->getMessage();
                    }
                } else {
                    return "В одной из групп не заполнен режим";
                }
                
                if ($row[3]) {
                    try {
                        $age = $this->groupsAge->where('age', $row[3])->pluck('id')->first();
                    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                        return "В одной из групп неправильно заполнена возрастная группа";
                        return $e->getMessage();
                    }
                } else {
                    return "В одной из групп не заполнена возрастная группа";
                }

                $group = new Groups([
                    'name' => $name,
                    'id_org' => $this->orgId,
                    'count_place' => $row[1],
                    'type_group' => $typeGroup,
                    'age' => $age,
                    'location' => ($row[4]) ? $row[4] : null
                ]);

                array_push($this->groupsImported, $name);

                return $group;
            }
        }
    }

    public function startRow(): int
    {
        return self::START_ROW;
    }
}
