<?php
namespace App\Imports;

use App\Models\SputnikFinance;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Contracts\Queue\ShouldQueue;

class SputnikFinanceImport implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading, ShouldQueue
{
    const CHUNK = 500;
    const START_ROW = 2;

    public function model(array $row) 
    {
        return new SputnikFinance([
            'uid' => $row[0],
            'company' => $row[1],
            'brand' => $row[2],
            'area' => $row[3],
            'income_from_doc' => $row[4],
            'frequency' => $row[5] ?? 0,
            'attendance' => $row[6] ?? 0,
            'rent_capacity' => $row[7] ?? 0,
            'pay_to_one' => $row[8]
        ]);
    }

    public function startRow(): int
    {
        return self::START_ROW;
    }

    public function batchSize(): int
    {
        return self::CHUNK;
    }

    public function chunkSize(): int
    {
        return self::CHUNK;
    }
}
