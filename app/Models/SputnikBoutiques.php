<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SputnikBoutiques extends Model
{
    use HasFactory;

    protected $table = "sputnik_boutiques";

    protected $fillable = [ 
        'uid', 
        "teach_area",
        "name", 
        "floor", 
        "ip_name", 
        "brand",
        "area",
        "rent", 
        "base_rate",
        "exploitation_rate",
        "rate",
        "total"
    ]; 
}
