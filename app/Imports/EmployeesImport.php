<?php

namespace App\Imports;

use App\Models\User;
use App\Models\Employees;
use App\Models\EmployeesToDay;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class EmployeesImport implements ToModel, WithStartRow
{
    const START_ROW = 2;
    protected $orgId;
    protected $employees;
    protected $excelIin;

    public function __construct($orgId)
    {
        $this->orgId = $orgId;
        $this->employees = Employees::where('id_org', $orgId)->pluck('iin')->toArray();
        $this->excelIin = [];
    }

    public function model(array $row)
    {
        if ($row[0] != "") {
            
            if (!in_array($row[1], $this->employees) && !in_array($row[1], $this->excelIin)) {

                $user = new User();
                $user->email = $row[1];
                $user->role = 3;
                $user->password = bcrypt("123qweQ!");
                $user->save();

                $excelDate = ($row[5] - 25569) * 86400; 
                $birthDate = date("Y-m-d", $excelDate);

                $name = mb_strtolower($row[0], 'UTF-8');
                $name = mb_convert_case($name, MB_CASE_TITLE, 'UTF-8');

                $this->excelIin[] += (string)$row[1];

                $employee = new Employees([
                    'name' => $name,
                    'id_org' => $this->orgId,
                    'iin' => $row[1],
                    'telephone' => $row[2],
                    'email' => $row[3],
                    'job' => $row[4],
                    'birth_date' => $birthDate,
                    'id_group' => "[]",
                ]);
                $employee->save();
                
                $employeesToDay = new EmployeesToDay([
                    'id_employee' => $employee->id,
                    'job' => $row[4],
                    'id_org' => $this->orgId,
                    'month' => Carbon::now()->format('Y-m')."-01"
                ]);
                $employeesToDay->save();
                
                return [$employee, $employeesToDay];
            }
        }
    }

    public function startRow(): int
    {
        return self::START_ROW;
    }
}
