"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_home_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=script&lang=js":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=script&lang=js ***!
  \***********************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _charts_chart_options_reason_of_missing_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../charts/chart_options_reason_of_missing.json */ "./resources/js/views/charts/chart_options_reason_of_missing.json");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/components/table/table.js");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/components/button/button.js");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/components/pagination/pagination.js");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/components/form-input/form-input.js");
/* harmony import */ var _components_Nav__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/Nav */ "./resources/js/components/Nav.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/lib/axios.js");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





// import apexchart from 'vue-apexcharts'


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Home",
  components: {
    Nav: _components_Nav__WEBPACK_IMPORTED_MODULE_1__["default"],
    axios: axios__WEBPACK_IMPORTED_MODULE_3__["default"],
    moment: (moment__WEBPACK_IMPORTED_MODULE_2___default()),
    apexchart: apexchart,
    BTable: bootstrap_vue__WEBPACK_IMPORTED_MODULE_4__.BTable,
    BButton: bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__.BButton,
    BPagination: bootstrap_vue__WEBPACK_IMPORTED_MODULE_6__.BPagination,
    BFormInput: bootstrap_vue__WEBPACK_IMPORTED_MODULE_7__.BFormInput
  },
  data: function data() {
    return {
      screenWidth: window.innerWidth,
      childs: [],
      activeIndex: 8,
      perPage: 10,
      currentPage: 1,
      rows: 1,
      filter: null,
      fields: [{
        key: 'id_child',
        label: 'ФИО',
        sortable: true
      }, {
        key: 'id_group',
        label: 'Группа',
        sortable: true
      }, {
        key: 'teachers',
        label: 'Воспитатели',
        sortable: true
      }, {
        key: 'time',
        label: 'Время',
        sortable: true
      }],
      chartsOptionsReasonOfMissing: _charts_chart_options_reason_of_missing_json__WEBPACK_IMPORTED_MODULE_0__,
      selectedGroup: 0,
      typeOfAttendance: 8,
      childsWithStatus: '',
      currentDayAttendance: '',
      teachers: '',
      groups: '',
      currentTime: '00:00',
      currentDay: '2023-01-01'
    };
  },
  watch: {
    'typeOfAttendance': function typeOfAttendance() {
      this.getChilds();
    },
    'selectedGroup': function selectedGroup() {
      this.getChilds();
      this.getChildsWithStatus();
    }
  },
  computed: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_8__.mapGetters)(["user"])), {}, {
    checkWidth: function checkWidth() {
      if (this.screenWidth > 1280) return true;
    },
    currentDayAttend: function currentDayAttend() {
      return this.childsWithStatus;
    },
    chartsReasonOfMissing: function chartsReasonOfMissing() {
      this.chartsOptionsReasonOfMissing.series = [this.currentDayAttend['missed'], this.currentDayAttend['absent'], this.currentDayAttend['attended']];
      // this.chartsOptionsReasonOfMissing.series = [this.currentDayAttend['missed'], this.currentDayAttend['absent'], this.currentDayAttend['attended']];
      this.chartsOptionsReasonOfMissing.chartOptions = {
        chart: {
          type: "donut"
        },
        plotOptions: {
          pie: {
            donut: {
              size: '90%',
              labels: {
                show: true,
                total: {
                  show: true,
                  showAlways: true,
                  label: 'Всего:',
                  fontSize: 32,
                  color: '#666666',
                  fontWeight: '700'
                }
              }
            }
          }
        },
        dataLabels: {
          enabled: false
        },
        colors: ['#B1B1B7', '#EDA774', '#4F81BD'],
        labels: ["Пропуск", "Отсутствуют", "Присутствуют"],
        legend: {
          show: false
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: '100%'
            },
            legend: {
              show: false
            }
          }
        }]
      };
      return this.chartsOptionsReasonOfMissing;
    }
  }),
  methods: {
    goToGroupsPage: function goToGroupsPage() {
      this.$router.push("/groups");
    },
    goToTeachersPage: function goToTeachersPage() {
      this.$router.push("/teachers");
    },
    goToChildsPage: function goToChildsPage() {
      this.$router.push("/childs");
    },
    filledProgressbar: function filledProgressbar(childs, allChilds) {
      var percent = Number(childs / allChilds * 100);
      if (percent > 100) percent = 100;
      return "width:" + percent + "%;";
    },
    filterByAttendanceClass: function filterByAttendanceClass(amount, type_of_attendance) {
      if (amount == 0) {
        return "gray_text";
      } else {
        if (this.activeIndex == type_of_attendance) {
          return "clicked";
        } else {
          return "green_text";
        }
      }
    },
    selectTypeOfAttendance: function selectTypeOfAttendance(amount, typeOfAttendance) {
      if (amount !== 0) {
        this.activeIndex = typeOfAttendance;
        this.typeOfAttendance = typeOfAttendance;
      }
    },
    selectGroup: function selectGroup(event) {
      this.selectedGroup = event.target.value;
    },
    selectTOA: function selectTOA(event) {
      this.typeOfAttendance = event.target.value;
    },
    getChilds: function getChilds() {
      var _this = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var res;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios__WEBPACK_IMPORTED_MODULE_3__["default"].get('/api/get-childs-statused?type_of_attendance=' + _this.typeOfAttendance + '&id_group=' + _this.selectedGroup);
            case 2:
              res = _context.sent;
              _this.childs = res.data;
              _this.rows = _this.childs.length;
            case 5:
            case "end":
              return _context.stop();
          }
        }, _callee);
      }))();
    },
    getTeachers: function getTeachers() {
      var _this2 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
        var res;
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return axios__WEBPACK_IMPORTED_MODULE_3__["default"].get('/get-teachers');
            case 2:
              res = _context2.sent;
              if (res.data.error == 'No user found!') {
                location.reload();
              }
              _this2.teachers = res.data;
            case 5:
            case "end":
              return _context2.stop();
          }
        }, _callee2);
      }))();
    },
    getGroups: function getGroups() {
      var _this3 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
        var res;
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return axios__WEBPACK_IMPORTED_MODULE_3__["default"].get('/api/get-org-groups');
            case 2:
              res = _context3.sent;
              _this3.groups = res.data;
            case 4:
            case "end":
              return _context3.stop();
          }
        }, _callee3);
      }))();
    },
    getChildsWithStatus: function getChildsWithStatus() {
      var _this4 = this;
      return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
        var res;
        return _regeneratorRuntime().wrap(function _callee4$(_context4) {
          while (1) switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return axios__WEBPACK_IMPORTED_MODULE_3__["default"].get('/api/get-childs-with-status?id_group=' + _this4.selectedGroup);
            case 2:
              res = _context4.sent;
              _this4.childsWithStatus = res.data;
            case 4:
            case "end":
              return _context4.stop();
          }
        }, _callee4);
      }))();
    },
    updateCurrentTime: function updateCurrentTime() {
      this.currentTime = moment__WEBPACK_IMPORTED_MODULE_2___default()().format('HH:mm');
    },
    updateCurrentDay: function updateCurrentDay() {
      this.currentDay = moment__WEBPACK_IMPORTED_MODULE_2___default()().format('DD.MM.YYYY');
    },
    updateScreenWidth: function updateScreenWidth() {
      this.screenWidth = window.innerWidth;
    } // moveSlide(e, direct) {
    //   var slider = document.getElementById('slider_wrapper');
    //   var sliderPosition = window.getComputedStyle(slider).getPropertyValue('left');
    //   var slideWidth = slider.firstElementChild.clientWidth;
    //   var marginWidth = window.getComputedStyle(slider.firstElementChild).getPropertyValue('margin-right');
    //   var paddingWidth = window.getComputedStyle(slider.firstElementChild).getPropertyValue('padding-right');
    //   var amountOfChild = slider.children.length;
    //   var lastChildPosition = slider.clientWidth * -1 + (amountOfChild - 4) * (slideWidth + parseFloat(marginWidth));
    //   console.log(sliderPosition, lastChildPosition);
    //   if ( sliderPosition == 0 && direct == 1) {
    //       slider.style.left = lastChildPosition;
    //   } else if (sliderPosition == lastChildPosition) {
    //       slider.style.left = 0;
    //   } else {
    //       var movedWidth = parseFloat(sliderPosition) + ((slideWidth + parseFloat(marginWidth) + parseFloat(paddingWidth)) * direct) + 'px';
    //       slider.style.left = movedWidth;
    //   }
    // }
  },
  mounted: function mounted() {
    var _this5 = this;
    return _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5() {
      return _regeneratorRuntime().wrap(function _callee5$(_context5) {
        while (1) switch (_context5.prev = _context5.next) {
          case 0:
            localStorage.setItem('token', localStorage.getItem('token'));
            _this5.updateCurrentTime();
            _this5.updateCurrentDay();
            _context5.next = 5;
            return _this5.getTeachers();
          case 5:
            _context5.next = 7;
            return _this5.getChilds();
          case 7:
            _context5.next = 9;
            return _this5.getGroups();
          case 9:
            _context5.next = 11;
            return _this5.getChildsWithStatus();
          case 11:
            window.addEventListener('resize', _this5.updateScreenWidth);
            setInterval(_this5.updateCurrentTime, 1000);
          case 13:
          case "end":
            return _context5.stop();
        }
      }, _callee5);
    }))();
  },
  beforeUnmount: function beforeUnmount() {
    window.removeEventListener('resize', this.updateScreenWidth);
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _public_images_teacher_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../public/images/teacher.png */ "./public/images/teacher.png");
/* harmony import */ var _public_images_teacher_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_public_images_teacher_png__WEBPACK_IMPORTED_MODULE_2__);
// Imports



var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_teacher_png__WEBPACK_IMPORTED_MODULE_2___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.home_empty_childs {\n  padding: 20px;\n}\n.clicked {\n  background-color: #2EA263;\n  color: #fff !important;\n  border-radius: 10px;\n  border: none;\n}\n.main_page {\n  height: auto;\n  display: flex;\n}\n.main_left {\n  width: 30%;\n}\n.main_right {\n  width: 69%;\n  margin-left: 1%;\n}\n.main_current_date {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  align-content: center;\n  width: 100%;\n  height: 20%;\n  background-color: #fff;\n  border-radius: 10px;\n  padding: 25px;\n  margin-bottom: 3%;\n  border-radius: 10px;\n  box-shadow: 0px 1px 8px -1px rgba(0, 0, 0, 0.20);\n}\n.main_current_time {\n  font-size: 6rem;\n  font-weight: 700;\n  line-height: 100px;\n  letter-spacing: 0.095em;\n  text-align: center;\n  color: #434656;\n}\n.main_current_day {\n  font-size: 2rem;\n  font-weight: 400;\n  letter-spacing: 0em;\n  text-align: center;\n  color: #434656B2;\n}\n.main_attendance_wrapper {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  background-color: #fff;\n  width: 100%;\n  height: 35%;\n  padding: 30px;\n  border-radius: 10px;\n  box-shadow: 0px 1px 8px -1px rgba(0, 0, 0, 0.20);\n}\n.main_childs_table {\n  height: 54%;\n  margin-top: 2%;\n  background-color: #fff;\n  border-radius: 10px;\n  box-shadow: 0px 1px 8px -1px rgba(0, 0, 0, 0.20);\n}\n.main_attendance_graph {\n  width: 60%;\n}\n.main_attendance_tables {\n  width: 40%;\n}\n.main_attendance_no_wrapper {\n  padding: 5px;\n  border-bottom: 1px solid #43465640;\n}\n.main_attendance_no {\n  border-radius: 10px;\n  border-left: 4px solid #C79F25;\n  box-shadow: 0px 1px 8px -1px #00000033;\n}\n.main_attendance_amount {\n  font-size: 2rem;\n  font-weight: 500;\n  line-height: 43px;\n  letter-spacing: 0em;\n  text-align: right;\n}\n.main_attendance_yes {\n  border-radius: 10px;\n  border-left: 4px solid #2EA263;\n  padding: 5px;\n  box-shadow: 0px 1px 8px -1px #00000033;\n}\n.main_attendance_title {\n  font-size: 0.875rem;\n  font-weight: 400;\n  line-height: 17px;\n  letter-spacing: 0em;\n  text-align: right;\n}\n.main_progressbar {\n  position: relative;\n  width: 82%;\n  height: 16px;\n  border-radius: 10px;\n  background-color: #C3D3DC;\n}\n.main_progressbar_filled {\n  height: 16px;\n  position: absolute;\n  background-color: #2EA263;\n  border-radius: 10px;\n}\n.main_groups {\n  width: 100%;\n  height: 100%;\n  overflow: scroll;\n}\n.main_groups_title {\n  display: flex;\n  align-items: center;\n  height: 6%;\n  font-size: 2rem;\n  font-weight: 400;\n  line-height: 36px;\n  letter-spacing: 0em;\n  text-align: left;\n  color: #434656B2;\n}\n.main_groups_wrapper {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  align-content: center;\n  width: 100%;\n  background-color: #fff;\n  border-radius: 10px;\n  padding: 25px;\n}\n.main_attendance_by_prop {\n  padding: 0 10px;\n}\n.main_attendance_reason {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  align-content: center;\n}\n.main_attendance_reason_title {\n  color: #434656B2;\n  font-size: 0.875rem;\n  font-weight: 400;\n  line-height: 17px;\n  letter-spacing: 0em;\n  text-align: center;\n}\n.main_attendance_reason_amount {\n  color: #434656;\n  font-size: 1.5rem;\n  font-weight: 500;\n  line-height: 28px;\n  letter-spacing: 0em;\n  text-align: right;\n}\n.main_teachers_header {\n  display: flex;\n  justify-content: space-between;\n  align-content: center;\n  align-items: center;\n}\n.main_teachers {\n\t/* position: relative; */\n\t/* height: 250px !important; */\n\toverflow: scroll;\n  height: 25%;\n  margin-bottom: 2%;\n}\n.slider_wrapper {\n\tdisplay: flex;\n  flex-direction: row;\n\tposition: absolute;\n\ttop: 0;\n\tleft: 0;\n}\n.main_teachers_blocks {\n  min-width: 23.5%;\n  margin-right: 1.5%;\n  display: flex;\n  flex-direction: row;\n}\n.main_teachers_block {\n  width: 100%;\n  background-color: #fff;\n  border-radius: 10px;\n}\n.main_teachers_info {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n  align-content: center;\n  align-items: center;\n  border-bottom: 1px solid #43465640;\n  padding: 10px;\n}\n.main_teachers_photo {\n  background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n  background-repeat: no-repeat;\n  background-position: center;\n  width: 40%;\n  height: 80px;\n  margin-right: 10px;\n}\n.main_teachers_phone {\n  color: #434656B2;\n  font-size: 0.875rem;\n  font-weight: 400;\n  line-height: 17px;\n  letter-spacing: 0em;\n  text-align: left;\n}\n.main_teachers_group {\n  padding: 10px;\n}\n.main_teachers_group_info {\n  display: flex;\n  justify-content: space-between;\n  align-content: center;\n  align-items: center;\n}\n.main_teachers_group_title {\n  font-size: 0.75rem;\n  font-weight: 400;\n  line-height: 14px;\n  letter-spacing: 0em;\n  text-align: left;\n}\n.main_teachers_group_name {\n  font-size: 0.875rem;\n  font-weight: 400;\n  line-height: 17px;\n  letter-spacing: 0em;\n  text-align: center;\n}\n.main_teachers_prop {\n  width: 60%;\n}\n.main_groups_nothing {\n  color: #434656B2;\n  font-size: 1rem;\n  font-weight: 400;\n  line-height: 19px;\n  letter-spacing: 0em;\n  text-align: left;\n}\n.main_childs_filter {\n  height: 5%;\n  justify-content: space-between;\n}\n.main_childs_status {\n  cursor: pointer;\n  padding: 5px;\n  margin: 4px;\n  border: 1px solid #EFF3F5;\n  /* margin-right: 5px; */\n}\n.main_childs_status:hover{\n  box-sizing: border-box;\n  color: #2EA263;\n  border-radius: 10px;\n  border: 1px solid #2EA263;\n}\n.main_childs_groups {\n  width: 20%;\n  padding: 10px;\n}\n.slider_arrows {\n  display: flex;\n}\n.slider_arrow {\n  cursor: pointer;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  align-content: center;\n  padding: 10px;\n  border-radius: 10px;\n  background-color: #434656;\n  margin-left: 20px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3ce4d0e1_lang_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3ce4d0e1_lang_css__WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3ce4d0e1_lang_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/views/home/index.vue":
/*!*******************************************!*\
  !*** ./resources/js/views/home/index.vue ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_3ce4d0e1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=3ce4d0e1 */ "./resources/js/views/home/index.vue?vue&type=template&id=3ce4d0e1");
/* harmony import */ var _index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js */ "./resources/js/views/home/index.vue?vue&type=script&lang=js");
/* harmony import */ var _index_vue_vue_type_style_index_0_id_3ce4d0e1_lang_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css */ "./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_3ce4d0e1__WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_3ce4d0e1__WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/home/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/home/index.vue?vue&type=script&lang=js":
/*!*******************************************************************!*\
  !*** ./resources/js/views/home/index.vue?vue&type=script&lang=js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=script&lang=js");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_3ce4d0e1_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=style&index=0&id=3ce4d0e1&lang=css");


/***/ }),

/***/ "./resources/js/views/home/index.vue?vue&type=template&id=3ce4d0e1":
/*!*************************************************************************!*\
  !*** ./resources/js/views/home/index.vue?vue&type=template&id=3ce4d0e1 ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3ce4d0e1__WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   staticRenderFns: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3ce4d0e1__WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3ce4d0e1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=3ce4d0e1 */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=template&id=3ce4d0e1");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=template&id=3ce4d0e1":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/home/index.vue?vue&type=template&id=3ce4d0e1 ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render),
/* harmony export */   staticRenderFns: () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Nav"),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "container_childs min-vh-90 max-content main_page" },
        [
          _c("div", { staticClass: "main_left" }, [
            _c("div", { staticClass: "main_current_date" }, [
              _c("div", { staticClass: "main_current_time" }, [
                _vm._v(" " + _vm._s(_vm.currentTime))
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "main_current_day" }, [
                _vm._v(" " + _vm._s(_vm.currentDay) + " ")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "main_attendance_wrapper" }, [
              _c(
                "div",
                { staticClass: "main_attendance_graph w-100 h-100" },
                [
                  _c("apexchart", {
                    attrs: {
                      type: "donut",
                      height: "100%",
                      options: _vm.chartsReasonOfMissing.chartOptions,
                      series: _vm.chartsReasonOfMissing.series
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "main_attendance_tables" }, [
                _c("div", { staticClass: "main_attendance_no mb-3" }, [
                  _c("div", { staticClass: "main_attendance_no_wrapper" }, [
                    _c("div", { staticClass: "gray_sm_text text-right" }, [
                      _vm._v("Отсутствуют")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "main_attendance_amount" }, [
                      _vm._v(" " + _vm._s(_vm.childsWithStatus.absent) + " ")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "main_attendance_by_prop" }, [
                    _c("div", { staticClass: "main_attendance_reason" }, [
                      _c(
                        "div",
                        { staticClass: "main_attendance_reason_title" },
                        [_vm._v(" Болеют ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "main_attendance_reason_amount" },
                        [_vm._v(" " + _vm._s(_vm.childsWithStatus.sick) + " ")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "main_attendance_reason" }, [
                      _c(
                        "div",
                        { staticClass: "main_attendance_reason_title" },
                        [_vm._v(" В отпуске ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "main_attendance_reason_amount" },
                        [
                          _vm._v(
                            " " + _vm._s(_vm.childsWithStatus.weekend) + " "
                          )
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "main_attendance_reason" }, [
                      _c(
                        "div",
                        { staticClass: "main_attendance_reason_title" },
                        [_vm._v(" Без причины ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "main_attendance_reason_amount" },
                        [
                          _vm._v(
                            " " + _vm._s(_vm.childsWithStatus.no_reason) + " "
                          )
                        ]
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "main_attendance_yes" }, [
                  _c("div", { staticClass: "gray_sm_text text-right" }, [
                    _vm._v(" Присутствуют ")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "main_attendance_amount" }, [
                    _vm._v(" " + _vm._s(_vm.childsWithStatus.attended) + " ")
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "main_groups_title",
                staticStyle: { margin: "3% 0" }
              },
              [_vm._v("Группы")]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "main_groups_wrapper" }, [
              _vm.groups.length !== 0
                ? _c(
                    "div",
                    { staticClass: "main_groups" },
                    [
                      _c("div", { staticClass: "main_groups_blocks" }, [
                        _c("div", { staticClass: "main_groups_block mt-3" }, [
                          _c("div", { staticClass: "normal_text text-left" }, [
                            _vm._v(" Заполненность организации")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "main_progressbar_wrapper flex justify-content-space-between align-items-center"
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass: "main_progressbar mr-2 relative"
                                },
                                [
                                  _c("div", {
                                    staticClass: "main_progressbar_filled",
                                    style: _vm.filledProgressbar(
                                      _vm.user.fact_amount,
                                      _vm.user.project_capacity
                                    )
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "normal_text" }, [
                                _vm._v(
                                  _vm._s(_vm.user.fact_amount) +
                                    " / " +
                                    _vm._s(_vm.user.project_capacity)
                                )
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.groups, function(group, j) {
                        return _c(
                          "div",
                          { key: j, staticClass: "main_groups_blocks" },
                          [
                            _c(
                              "div",
                              { staticClass: "main_groups_block mt-3" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "normal_text text-left" },
                                  [_vm._v(" " + _vm._s(group.name))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "main_progressbar_wrapper flex justify-content-space-between align-items-center"
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "main_progressbar mr-2 relative"
                                      },
                                      [
                                        _c("div", {
                                          staticClass:
                                            "main_progressbar_filled",
                                          style: _vm.filledProgressbar(
                                            group.childs_with_status,
                                            group.count_place
                                          )
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "normal_text" }, [
                                      _vm._v(
                                        _vm._s(group.childs_with_status) +
                                          " / " +
                                          _vm._s(group.childs_amount)
                                      )
                                    ])
                                  ]
                                )
                              ]
                            )
                          ]
                        )
                      })
                    ],
                    2
                  )
                : _c("div", { staticClass: "main_groups" }, [
                    _c("div", { staticClass: "main_groups_nothing mb-3" }, [
                      _vm._v("Нет ни одной группы детей")
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "teachers_no_button teachers_button",
                        on: {
                          click: function($event) {
                            return _vm.goToGroupsPage()
                          }
                        }
                      },
                      [_vm._v("  Создать группы")]
                    )
                  ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "main_right" }, [
            _c("div", { staticClass: "main_teachers_wrapper w-100 mb-3" }, [
              _vm._m(0),
              _vm._v(" "),
              _vm.teachers.length !== 0
                ? _c(
                    "div",
                    { staticClass: "main_teachers w-100 flex" },
                    _vm._l(_vm.teachers, function(teacher, index) {
                      return _c(
                        "div",
                        { key: index, staticClass: "main_teachers_blocks" },
                        [
                          _c("div", { staticClass: "main_teachers_block" }, [
                            _c("div", { staticClass: "main_teachers_info" }, [
                              _c("div", { staticClass: "main_teachers_photo" }),
                              _vm._v(" "),
                              _c("div", { staticClass: "main_teachers_prop" }, [
                                _c(
                                  "div",
                                  { staticClass: "normal_sm_text mb-2" },
                                  [_vm._v(" " + _vm._s(teacher.name))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "main_teachers_phone" },
                                  [
                                    _vm._v(
                                      " " + _vm._s(teacher.telephone) + " "
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "main_teachers_group" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "gray_sm_text mb-2" },
                                  [_vm._v("Закрепленные группы")]
                                ),
                                _vm._v(" "),
                                _vm._l(teacher.group_names, function(
                                  groupName,
                                  i
                                ) {
                                  return _c(
                                    "div",
                                    {
                                      key: i,
                                      staticClass:
                                        "main_teachers_group_infos flex align-items-center"
                                    },
                                    [
                                      _c("div", { staticClass: "groups_logo" }),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "main_teachers_group_name"
                                        },
                                        [
                                          _vm._v(
                                            " " + _vm._s(groupName.group_name)
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ]
                      )
                    }),
                    0
                  )
                : _c("div", { staticClass: "main_groups" }, [
                    _c("div", { staticClass: "main_groups_nothing mb-3" }, [
                      _vm._v("Нет ни одного воспитателя")
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "teachers_no_button teachers_button",
                        on: {
                          click: function($event) {
                            return _vm.goToTeachersPage()
                          }
                        }
                      },
                      [_vm._v("  Добавить воспитателей")]
                    )
                  ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "main_childs_filter flex align-items-center" },
                [
                  _vm.checkWidth
                    ? _c(
                        "div",
                        {
                          staticClass:
                            "main_childs_statuses flex align-items-center"
                        },
                        [
                          _c(
                            "div",
                            {
                              staticClass: "main_childs_status gray_text",
                              class: _vm.filterByAttendanceClass(
                                _vm.childsWithStatus.missed,
                                8
                              ),
                              on: {
                                click: function($event) {
                                  return _vm.selectTypeOfAttendance(
                                    _vm.childsWithStatus.missed,
                                    8
                                  )
                                }
                              }
                            },
                            [
                              _vm._v(
                                " \n              Не отмечены ( " +
                                  _vm._s(_vm.childsWithStatus.missed) +
                                  " )\n            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "main_childs_status gray_text",
                              class: _vm.filterByAttendanceClass(
                                _vm.childsWithStatus.absent +
                                  _vm.childsWithStatus.attended,
                                7
                              ),
                              on: {
                                click: function($event) {
                                  _vm.selectTypeOfAttendance(
                                    _vm.childsWithStatus.attended +
                                      _vm.childsWithStatus.absent,
                                    7
                                  )
                                }
                              }
                            },
                            [
                              _vm._v(
                                " \n              Oтмечены ( " +
                                  _vm._s(
                                    _vm.childsWithStatus.absent +
                                      _vm.childsWithStatus.attended
                                  ) +
                                  " )\n            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "main_childs_status gray_text",
                              class: _vm.filterByAttendanceClass(
                                _vm.childsWithStatus.sick,
                                4
                              ),
                              on: {
                                click: function($event) {
                                  return _vm.selectTypeOfAttendance(
                                    _vm.childsWithStatus.sick,
                                    4
                                  )
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n              Болеют ( " +
                                  _vm._s(_vm.childsWithStatus.sick) +
                                  " )\n            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "main_childs_status gray_text",
                              class: _vm.filterByAttendanceClass(
                                _vm.childsWithStatus.weekend,
                                5
                              ),
                              on: {
                                click: function($event) {
                                  return _vm.selectTypeOfAttendance(
                                    _vm.childsWithStatus.weekend,
                                    5
                                  )
                                }
                              }
                            },
                            [
                              _vm._v(
                                " \n              В отпуске ( " +
                                  _vm._s(_vm.childsWithStatus.weekend) +
                                  " )\n            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "main_childs_status gray_text",
                              class: _vm.filterByAttendanceClass(
                                _vm.childsWithStatus.no_reason,
                                6
                              ),
                              on: {
                                click: function($event) {
                                  return _vm.selectTypeOfAttendance(
                                    _vm.childsWithStatus.no_reason,
                                    6
                                  )
                                }
                              }
                            },
                            [
                              _vm._v(
                                " \n              Отсутствуют ( " +
                                  _vm._s(_vm.childsWithStatus.no_reason) +
                                  " )\n            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "main_childs_status gray_text",
                              class: _vm.filterByAttendanceClass(
                                _vm.childsWithStatus.attended,
                                2
                              ),
                              on: {
                                click: function($event) {
                                  return _vm.selectTypeOfAttendance(
                                    _vm.childsWithStatus.attended,
                                    2
                                  )
                                }
                              }
                            },
                            [
                              _vm._v(
                                " \n              Присутствуют ( " +
                                  _vm._s(_vm.childsWithStatus.attended) +
                                  " )\n            "
                              )
                            ]
                          )
                        ]
                      )
                    : _c("div", { staticClass: "main_childs_groups" }, [
                        _c(
                          "select",
                          {
                            staticClass: "w-100",
                            attrs: { required: "" },
                            on: {
                              "!change": function($event) {
                                return _vm.selectTOA($event)
                              }
                            }
                          },
                          [
                            _c("option", { domProps: { value: 8 } }, [
                              _vm._v(" Не отмечены ")
                            ]),
                            _vm._v(" "),
                            _c("option", { domProps: { value: 7 } }, [
                              _vm._v(" Oтмечены ")
                            ]),
                            _vm._v(" "),
                            _c("option", { domProps: { value: 4 } }, [
                              _vm._v(" Болеют ")
                            ]),
                            _vm._v(" "),
                            _c("option", { domProps: { value: 5 } }, [
                              _vm._v(" В отпуске ")
                            ]),
                            _vm._v(" "),
                            _c("option", { domProps: { value: 6 } }, [
                              _vm._v(" Отсутствуют ")
                            ]),
                            _vm._v(" "),
                            _c("option", { domProps: { value: 2 } }, [
                              _vm._v(" Присутствуют ")
                            ])
                          ]
                        )
                      ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "main_childs_groups" }, [
                    _c(
                      "select",
                      {
                        staticClass: "w-100",
                        attrs: { required: "" },
                        on: {
                          "!change": function($event) {
                            return _vm.selectGroup($event)
                          }
                        }
                      },
                      [
                        _c("option", { domProps: { value: 0 } }, [
                          _vm._v(" Все группы ")
                        ]),
                        _vm._v(" "),
                        _vm._l(_vm.groups, function(group, k) {
                          return _c(
                            "option",
                            {
                              key: k,
                              staticStyle: { padding: "5px" },
                              domProps: { value: group.id }
                            },
                            [_vm._v(" " + _vm._s(group.name) + " ")]
                          )
                        })
                      ],
                      2
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "main_childs_table" }, [
                _c(
                  "div",
                  { staticClass: "childs_panel" },
                  [
                    _c("b-form-input", {
                      staticClass: "childs_search",
                      attrs: { type: "search", placeholder: "Поиск" },
                      model: {
                        value: _vm.filter,
                        callback: function($$v) {
                          _vm.filter = $$v
                        },
                        expression: "filter"
                      }
                    }),
                    _vm._v(" "),
                    _c("b-pagination", {
                      staticClass: "childs_pagination",
                      attrs: {
                        "total-rows": _vm.rows,
                        "per-page": _vm.perPage,
                        "aria-controls": "my-table",
                        "hide-goto-end-buttons": "",
                        "hide-ellipsis": ""
                      },
                      model: {
                        value: _vm.currentPage,
                        callback: function($$v) {
                          _vm.currentPage = $$v
                        },
                        expression: "currentPage"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "childs_table_wrapper" },
                  [
                    _c("b-table", {
                      attrs: {
                        "per-page": _vm.perPage,
                        "current-page": _vm.currentPage,
                        filter: _vm.filter,
                        items: _vm.childs,
                        fields: _vm.fields
                      }
                    }),
                    _vm._v(" "),
                    _vm.childsWithStatus.length == 0
                      ? _c(
                          "div",
                          {
                            staticClass: "home_empty_childs",
                            on: {
                              click: function($event) {
                                return _vm.goToChildsPage()
                              }
                            }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "main_groups_nothing mb-3" },
                              [_vm._v(" Нет ни одного ребенка ")]
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass:
                                  "teachers_no_button teachers_button",
                                on: {
                                  click: function($event) {
                                    return _vm.goToTeachersPage()
                                  }
                                }
                              },
                              [_vm._v(" Добавить детей")]
                            )
                          ]
                        )
                      : _vm._e()
                  ],
                  1
                )
              ])
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main_teachers_header" }, [
      _c(
        "div",
        { staticClass: "main_groups_title", staticStyle: { margin: "2% 0" } },
        [_vm._v("Воспитатели")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/charts/chart_options_reason_of_missing.json":
/*!************************************************************************!*\
  !*** ./resources/js/views/charts/chart_options_reason_of_missing.json ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = JSON.parse('{"chartOptions":{"chart":{"type":"donut"},"labels":["Пропуск","Больничный","Отпуск"],"legend":{"show":false},"responsive":[{"breakpoint":480,"options":{"chart":{"width":"100%"},"legend":{"show":false}}}]}}');

/***/ })

}]);