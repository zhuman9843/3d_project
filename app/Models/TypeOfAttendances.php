<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfAttendances extends Model
{
    use HasFactory;

    protected $table = "type_of_attendances";

    protected $fillable = ["attendance", "index_of_attendance", "coef_of_attendance"];
}
