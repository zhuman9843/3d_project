<?php

namespace Database\Seeders;

use App\Models\GroupsAgeRanges;
use Illuminate\Database\Seeder;

class GroupsAgeUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $age57 = new GroupsAgeRanges([
            'age' => 'от 3 до 5 лет'
        ]);
        $age57->save();
    }
}
