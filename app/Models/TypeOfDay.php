<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfDay extends Model
{
    use HasFactory;

    protected $table = "type_of_day";

    protected $fillable = ["type_of_day"];
}
