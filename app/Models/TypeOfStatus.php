<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfStatus extends Model
{
    use HasFactory;
    
    protected $table = "type_of_status";

    protected $fillable = ["status_text", "status_send"];

}
