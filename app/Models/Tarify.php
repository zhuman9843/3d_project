<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarify extends Model
{
    use HasFactory;

    protected $table = "tarify";

    protected $fillable = ["region", "group_id", "type_of_group", "type_of_region", "type_of_location", "tarify"];
}
