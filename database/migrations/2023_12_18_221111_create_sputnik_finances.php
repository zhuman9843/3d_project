<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSputnikFinances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sputnik_finances', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->string('company');
            $table->string('brand');
            $table->float('area')->round(2);
            $table->float('income_from_doc')->round(2);
            $table->float('frequency')->round(2);
            $table->float('attendance')->round(2);
            $table->float('rent_capacity')->round(2);
            $table->float('pay_to_one')->round(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sputnik_finances');
    }
}
