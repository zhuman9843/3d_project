<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_analytics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_org');
            $table->unsignedBigInteger('region');
            $table->string('department');
            $table->string('government');
            $table->unsignedBigInteger('bin');
            $table->string('contragent');
            $table->string('type_of_department');
            $table->date('month');
            $table->integer('group');
            $table->integer('child_amount_plan');
            $table->integer('child_amount_group');
            $table->integer('child_amount_fact');
            $table->integer('plan_child_to_day');
            $table->integer('fact_child_to_day');
            $table->integer('budget_plan');
            $table->integer('budget_fact');
            $table->integer('missing');
            $table->integer('sick');
            $table->integer('weekend');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_analytics');
    }
}
