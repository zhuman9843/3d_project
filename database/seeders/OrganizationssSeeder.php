<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrganizationssSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->insert([
            "type_project" => 1,
            "name" => "Еркем-ай  балапан  балабақшасы  мекемесі", 
            "bin" => 11112222, 
            "region" => 6, 
            "district" => 20, 
            "type_org" => 1, 
            "type_location" => 1, 
            "type_region" => 1, 
            "project_capacity" => 320, 
            "telephone" => 87786216023, 
            "admin_name" => "Гладкова Алина Максимовна", 
            "location" => "51.146861, 71.371630", 
            "email" => "erkemai@bb.kz", 
        ]);
        DB::table('organizations')->insert([
            "type_project" => 2,
            "name" => "Бақытты әлем", 
            "bin" => 11112222, 
            "region" => 6, 
            "district" => 20, 
            "type_org" => 1, 
            "type_location" => 1, 
            "type_region" => 1, 
            "project_capacity" => 80, 
            "telephone" => 87786216023, 
            "admin_name" => "Гладкова Алина Максимовна", 
            "location" => "51.146861, 71.371630", 
            "email" => "bakyt@bb.kz",
        ]);
    }
}
