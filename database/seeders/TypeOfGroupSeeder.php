<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOfGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_group')->insert([
            'type_of_group' => 'Неполный день',
            'class_of_type' => 'Общеобразовательный',
            'title_of_type' => 'Группы с неполным днем пребывания',
            'category' => 'gor0',
        ]);
        DB::table('type_of_group')->insert([
            'type_of_group' => '9 часовой',
            'class_of_type' => 'Общеобразовательный',
            'title_of_type' => 'Группы с 9 часовым режимом пребывания',
            'category' => 'gor9',
        ]);
        DB::table('type_of_group')->insert([
            'type_of_group' => '10,5 часовой',
            'class_of_type' => 'Общеобразовательный',
            'title_of_type' => 'Группы с 10,5 часовым режимом пребывания',
            'category' => 'gor10',
        ]);
        DB::table('type_of_group')->insert([
            'type_of_group' => 'Санаторная',
            'class_of_type' => 'Санаторная группа',
            'title_of_type' => 'Санаторная группа',
            'category' => 'grs',
        ]);
        DB::table('type_of_group')->insert([
            'type_of_group' => 'Коррекционная',
            'class_of_type' => 'Коррекционная группа',
            'title_of_type' => 'Коррекционная группа',
            'category' => 'grk',
        ]);
    }
}
