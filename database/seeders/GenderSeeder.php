<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Gender;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $genders = [
        [
            'gender' => 'Мужской',
            'gender_code' => 'male'
        ],
        [
            'gender' => 'Женский',
            'gender_code' => 'female'
        ]
    ];

    public function run()
    {
        foreach ( $this->genders as $gender) {
            $gen = new Gender();
            $gen->gender = $gender['gender'];
            $gen->gender_code = $gender['gender_code'];
            $gen->save();
        }
    }
}
