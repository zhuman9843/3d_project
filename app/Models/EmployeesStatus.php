<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeesStatus extends Model
{
    use HasFactory;

    protected $table = "employees_status";

    protected $fillable = ["id_employee", "id_org", "status_text", "status_send", "date"];
}
