<?php
namespace App\Imports;

use App\Models\SputnikBoutiques;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Contracts\Queue\ShouldQueue;

class SputnikRoomsImport implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading, ShouldQueue
{
    const CHUNK = 500;
    const START_ROW = 2;

    public function model(array $row) 
    {
        return new SputnikBoutiques([
            'uid' => $row[1],
            'teach_area' => $row[0],
            'name' => $row[2],
            'floor' => $row[3],
            'ip_name' => $row[4],
            'brand' => $row[5],
            'area' => $row[6],
            'base_rate' => $row[7],
            'exploitation_rate' => $row[8],
            'rate' => $row[9],
            'rent' => $row[10],
            'total' => $row[11]
        ]);
    }

    public function startRow(): int
    {
        return self::START_ROW;
    }

    public function batchSize(): int
    {
        return self::CHUNK;
    }

    public function chunkSize(): int
    {
        return self::CHUNK;
    }
}
