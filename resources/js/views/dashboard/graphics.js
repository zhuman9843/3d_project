import chartOptionsChildToDay from "./chart_options_child_to_day.json";
import chartOptionsAttendanceChildToDay from "./chart_options_attendance_child_to_day.json";
import chartOptionsBudgetChildToDay from "./chart_options_budget_child_to_day.json";
import chartOptionsBudgetDifferenceChildToDay from "./chart_options_budget_difference_child_to_day.json";
import chartsOptionsReasonOfMissing from "./chart_options_reason_of_missing.json";
import VueApexCharts from 'vue-apexcharts';
import Nav from "./../../components/Nav";
import * as XLSX from 'xlsx-js-style';

export default {
  name: 'ChildToDay',
  components: {
    Nav,
    XLSX,
    apexchart: VueApexCharts
  },
  data: () => ({
    chartOptionsChildToDay: chartOptionsChildToDay,
    chartOptionsAttendanceChildToDay: chartOptionsAttendanceChildToDay,
    chartOptionsBudgetChildToDay: chartOptionsBudgetChildToDay,
    chartOptionsBudgetDifferenceChildToDay: chartOptionsBudgetDifferenceChildToDay,
    chartsOptionsReasonOfMissing: chartsOptionsReasonOfMissing
  }),
  watch: {
  },
  computed:{
    chartsAttendanceChildToDay() {
      this.chartOptionsAttendanceChildToDay.chartOptions = {
        chart : {
          stacked : true
        },
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "количество",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              return (y).toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }
      let series = [];
      let sickData = {
        'name' : 'Болеют',
        'data' : []   
      };
      let weekendData = {
        'name' : 'Отпуск',
        'data' : []   
      };
      let noReasonData = {
        'name' : 'Пропуск',
        'data' : []   
      };
      for (let prop in this.chartAttendanceData) {
        let data = this.chartAttendanceData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          if (sickData.data[key]) {
            sickData.data[key] += value.sick;
          } else {
            sickData.data[key] = value.sick;
          }
          if (weekendData.data[key]) {
            weekendData.data[key] += value.weekend;
          } else {
            weekendData.data[key] = value.weekend;
          }
          if (noReasonData.data[key]) {
            noReasonData.data[key] += value.missing;
          } else {
            noReasonData.data[key] = value.missing;
          }
        });
      }
      sickData.data = Object.values(sickData.data);
      weekendData.data = Object.values(weekendData.data);
      noReasonData.data = Object.values(noReasonData.data);
      series.push(sickData);
      series.push(weekendData);
      series.push(noReasonData);
      this.chartOptionsAttendanceChildToDay.series = series
      this.chartOptionsAttendanceChildToDay.chartOptions.colors = ['#EDA774', '#4F81BD', '#B1B1B7'];
      return this.chartOptionsAttendanceChildToDay;
    },
    chartsChildToDay() {
      this.chartOptionsChildToDay.chartOptions = {
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "количество",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              return (y).toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }
      let series = [];
      let monthPlanData = {
        'name' : 'План',
        'data' : []
      };
      let monthFactData = {
        'name' : 'Факт',
        'data' : []   
      };
      for (let prop in this.chartData) {
        let data = this.chartData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          if (monthPlanData.data[key]) {
            monthPlanData.data[key] += value.plan_child_to_day;
          } else {
            monthPlanData.data[key] = value.plan_child_to_day;
          }
          if (monthFactData.data[key]) {
            monthFactData.data[key] += value.fact_child_to_day;
          } else {
            monthFactData.data[key] = value.fact_child_to_day;
          }
        });
      }
      monthPlanData.data = Object.values(monthPlanData.data);
      monthFactData.data = Object.values(monthFactData.data);
      series.push(monthPlanData);
      series.push(monthFactData);
      this.chartOptionsChildToDay.series = series;
      this.chartOptionsChildToDay.chartOptions.colors = ['#27AE60', '#D7C41E'];
      return this.chartOptionsChildToDay;
    },
    chartsBudgetChildToDay() {
      this.chartOptionsBudgetChildToDay.chartOptions = {
        chart : {
          stacked : false
        },
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "тыс. тенге",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              return (y).toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }
      let series = [];
      let monthPlanData = {
        'name' : 'План',
        'data' : []   
      };
      let monthFactData = {
        'name' : 'Факт',
        'data' : []   
      };
      for (let prop in this.chartData) {
        let data = this.chartData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          if (monthPlanData.data[key]) {
            monthPlanData.data[key] += value.budget_plan / 1000;
          } else {
            monthPlanData.data[key] = value.budget_plan / 1000;
          }
          if (monthFactData.data[key]) {
            monthFactData.data[key] += value.budget_fact / 1000
          } else {
            monthFactData.data[key] = value.budget_fact / 1000
          }
        });
      }
      monthPlanData.data = Object.values(monthPlanData.data);
      monthFactData.data = Object.values(monthFactData.data);
      series.push(monthPlanData);
      series.push(monthFactData);
      this.chartOptionsBudgetChildToDay.series = series;
      this.chartOptionsBudgetChildToDay.chartOptions.colors = ['#27AE60', '#D7C41E'];
      
      return this.chartOptionsBudgetChildToDay;
    },
    chartsBudgetDifferenceChildToDay() {
      this.chartOptionsBudgetDifferenceChildToDay.chartOptions = {
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "тыс. тенге",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              return (y).toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }
      let series = [];
      let differenceData = {
        'name' : 'План - Факт',
        'data' : []   
      };
      for (let prop in this.chartData) {
        let data = this.chartData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          if (differenceData.data[key]) {
          differenceData.data[key] += (value.budget_plan - value.budget_fact) / 1000;
          } else {
            differenceData.data[key] = (value.budget_plan - value.budget_fact) / 1000;
          }
        });
      }
      differenceData.data = Object.values(differenceData.data);
      series.push(differenceData);
      this.chartOptionsBudgetDifferenceChildToDay.series = series;
      this.chartOptionsBudgetDifferenceChildToDay.chartOptions.colors = ['#eda774'];
      
      return this.chartOptionsBudgetDifferenceChildToDay;
    },
    chartsReasonOfMissing() {
      this.chartsOptionsReasonOfMissing.series = [this.currentDayAttend.missed, this.currentDayAttend.sick, this.currentDayAttend.weekend];
      this.chartsOptionsReasonOfMissing.chartOptions = {
        plotOptions: {
          pie: {
            donut: {
              size: '80%',
              labels: {
                show: true,
                total: {
                  show: true,
                  showAlways: true,
                  label: 'Всего:',
                  fontSize: 32,
                  color: '#666666',
                  fontWeight: '700',
                }
              }
            }
          }
        },
        chart: {
          type: 'donut',
        },
        dataLabels: {
          enabled: false,
        },
        colors: ['#B1B1B7', '#EDA774', '#4F81BD'],
        labels: ["Пропуск", "Больничный", "Отпуск"],
        legend: {
          show: false
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: '100%'
            },
            legend: {
              show: false
            }
          },
        }]
      }
      return this.chartsOptionsReasonOfMissing;
    },
    methods: {
        
    },
  },
  created() {
  },
  mounted() {
  }
}