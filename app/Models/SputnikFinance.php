<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SputnikFinance extends Model
{
    use HasFactory;

    protected $table = "sputnik_finances";

    protected $fillable = [ 
        'uid', 
        "company",
        "brand",
        "area",
        "income_from_doc", 
        "frequency", 
        "attendance",
        "rent_capacity",
        "pay_to_one"
    ]; 
}
