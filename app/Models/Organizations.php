<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organizations extends Model
{
    use HasFactory;

    protected $table = "organizations";

    protected $fillable = ["type_project", "name", "bin", "region", "district", "type_org", "type_location", "type_region", "project_capacity", "telephone", "admin_name", "location", "address", "email", "created_at", "updated_at"];
}
