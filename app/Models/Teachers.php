<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
    use HasFactory;

    protected $table = "teachers";

    protected $fillable = ["name", "id_org", "id_group", "telephone", "iin", "birth_date", "email", "created_at", "updated_at"];
}
