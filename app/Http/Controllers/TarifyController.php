<?php

namespace App\Http\Controllers;

use App\Imports\SputnikFinanceImport;
use App\Imports\SputnikRoomsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TarifyImport;

class TarifyController extends Controller
{
    public function import( Request $request ) {

        $file = $request->file('file');

        Excel::import(new TarifyImport(), $file);

        return response()->json(['status' => 'success'], 200);
    }
    
    public function importSputnikRooms( Request $request ) {

        $file = $request->file('file');

        Excel::import(new SputnikRoomsImport(), $file);

        return response()->json(['status' => 'success'], 200);
    }
    
    public function importSputnikFinance( Request $request ) {

        $file = $request->file('file');

        Excel::import(new SputnikFinanceImport(), $file);

        return response()->json(['status' => 'success'], 200);
    }
}
