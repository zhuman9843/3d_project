<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Models\TableAnalyticUsers;
use App\Models\TableAnalytics;
use App\Models\Organizations;
use App\Models\Government;
use App\Models\Department;
use App\Models\TypeOrg;
use App\Models\FilteredOrganizations;
use App\Models\User;
use Carbon\Carbon;

class RegisterController extends Controller
{
    private $allMonths;

    public function __construct()
    {
        $currentYear = date('Y');
        for ($month = 1; $month <= 12; $month++) {
            $month = sprintf("%02d", filter_var($month, FILTER_SANITIZE_NUMBER_INT));
            $this->allMonths[] = date("$currentYear-$month-01");
        }  
    }

    public function index() 
    {
        return view('registration.index');
    }

    public function register(RegisterRequest $request)
    {
        $time = Carbon::now();

        $request->validate([
            "type_project" => 'required|numeric',
            "name" => 'required|string|max:255',
            "bin" => 'required|numeric|min:12',
            "telephone" => 'required|numeric',
            "admin_name" => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'location' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'password'  => 'required|min:3|confirmed'
        ]);

        $checkExistingBin = FilteredOrganizations::where('bin', $request->bin)->first();

        if (!$checkExistingBin) {
            return response()->json(['errors' => 'Организации с таким БИНом нету, обратитесь в службу поддержки!'], 200);
        }
        
        $checkExistingBinInOrg = Organizations::where('bin', $request->bin)->first();

        if ($checkExistingBinInOrg) {
            return response()->json(['errors' => 'Организация с таким БИНом уже зарегистрирована!'], 200);
        }

        $checkExistingEmail = User::where('email', $request->bin)->first();
        
        if ($checkExistingEmail) {
            return response()->json(['errors' => 'Организация с таким БИНом уже зарегистрирована!'], 200);
        }

        $organization = new Organizations();
        $organization->name = $request->name;
        $organization->bin = $request->bin;
        $organization->region = $request->region;
        $organization->district = $request->district;
        $organization->type_org = $request->type_org;
        $organization->type_location = $request->type_location;
        $organization->type_region = $request->type_region;
        $organization->type_project = $request->type_project;
        $organization->project_capacity = $request->project_capacity;
        $organization->telephone = $request->telephone;
        $organization->admin_name = $request->admin_name;
        $organization->location = $request->location;
        $organization->address = $request->address;
        $organization->email = $request->email;
        $organization->created_at = $time;
        $organization->updated_at = $time;

        $government = Government::where('region_id', $request->region)->pluck('government')->first();
        $department = Department::where('district_id', $request->district)->pluck('department')->first();
        $typeOfDepartment = TypeOrg::where('id', $request->type_org)->pluck('type_org')->first();

        $user = new User();
        $user->email = $request->bin;
        $user->password = bcrypt($request->password);
        $user->created_at = $time;
        $user->updated_at = $time;
        $user->save();
        $organization->save();

        foreach ($this->allMonths as $month) {
            $tableAnalytics = new TableAnalytics();
            $tableAnalytics->id_org = $organization->id;
            $tableAnalytics->region = $organization->region;
            $tableAnalytics->department = $department;
            $tableAnalytics->government = $government;
            $tableAnalytics->bin = $organization->bin;
            $tableAnalytics->contragent = $organization->name;
            $tableAnalytics->type_of_department = $typeOfDepartment;
            $tableAnalytics->month = $month;
            $tableAnalytics->group = 0;
            $tableAnalytics->child_amount_plan = $organization->project_capacity;
            $tableAnalytics->child_amount_group = 0;
            $tableAnalytics->child_amount_fact = 0;
            $tableAnalytics->plan_child_to_day = 0;
            $tableAnalytics->fact_child_to_day = 0;
            $tableAnalytics->budget_plan= 0;
            $tableAnalytics->budget_fact = 0;
            $tableAnalytics->missing = 0;
            $tableAnalytics->sick = 0;
            $tableAnalytics->weekend = 0;
            $tableAnalytics->save();
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function analyticUsersRegister (Request $request) 
    {
        $request->validate([
            "name" => 'required|string|max:255',
            "government" => 'required|numeric',
            "department" => 'required|numeric',
            'email' => 'required|email|max:255',
            'password'  => 'required|min:3|confirmed'
        ]);

        $time = Carbon::now();

        $user = new User();
        $analyticUsers = new TableAnalyticUsers();

        $analyticUsers->name = $request->name;
        $analyticUsers->admin_name = "";
        $analyticUsers->government_id = $request->government;
        $analyticUsers->department_id = $request->department;
        $analyticUsers->email = $request->email;
        $analyticUsers->created_at = $time;
        $analyticUsers->updated_at = $time;
        $analyticUsers->save();
        
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->created_at = $time;
        $user->updated_at = $time;
        $user->role = 2;
        $user->save();

        return response()->json(['status' => 'success'], 200);
    }
}
