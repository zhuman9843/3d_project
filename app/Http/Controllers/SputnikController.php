<?php

namespace App\Http\Controllers;

use App\Imports\SputnikRoomsImport;
use App\Models\SputnikBoutiques;
use App\Models\SputnikFinance;
use Illuminate\Http\Request;

class SputnikController extends Controller
{
    public $uid;

    public function getDataBoutique( Request $request ) {
        $this->uid = $request->input('uid');

        $financeData = SputnikFinance::where('uid', $this->uid)->first();

        return $financeData;
    }
    
    public function getFinanceDataBoutiques() {
        $boutiquesData = SputnikFinance::all()->toArray();
        return $boutiquesData;
    }

    public function getDataBoutiques() {
        $boutiquesData = SputnikBoutiques::all()->map( function( $boutique) {
            $value = $boutique->rent;
            $colorHex = '0xFF0000';

            if ($value >= 5000 && $value < 10000) {
              $colorHex = '0xFFA500'; 
            } else if ($value >= 10000 && $value < 20000) {
              $colorHex = '0xFFFF00';
            } else if ($value >= 20000 && $value < 30000) {
              $colorHex = '0x00FF00'; 
            } else if ($value >= 30000 && $value <= 50000) {
              $colorHex = '0x008000';
            } else if ($value > 50000) {
              $colorHex = '0x004d00';
            }

            $boutique->hex = $colorHex;
            $boutique->save;
            
            return $boutique;
        });

        return $boutiquesData;
    }
}
