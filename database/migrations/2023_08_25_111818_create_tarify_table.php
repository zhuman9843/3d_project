<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarifyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarify', function (Blueprint $table) {
            $table->id();
            $table->integer('region');
            $table->integer('group_id');
            $table->integer('type_of_group');
            $table->integer('type_of_region');
            $table->integer('type_of_location');
            $table->integer('tarify');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarify');
    }
}
