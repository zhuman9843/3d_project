<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;

class EmailNotification extends Mailable
{
    public $messageContent;

    public function __construct($messageContent)
    {
        $this->messageContent = $messageContent;
    }

    public function build()
    {
        return $this->view('emails.email-template')
                    ->with(['messageContent' => $this->messageContent]);
    }
}
