declare module 'vue2-google-maps' {
    import { Marker } from 'vue';
    import { GmapMap } from 'vue';
  
    export const GmapMap: GmapMap;
    export const Marker: Marker;
    // Add other exported components and types here
  }