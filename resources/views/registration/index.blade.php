<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css">

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVLfwNiqZ5O0Dmyi_56Rnb3aMkGOyJvcI&libraries=places"></script>
    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
</head>

<body>
    <div id="app">
        <registration></registration>
    </div>
</body>
<script src="{{ mix('/js/app.js') }}"></script>

</html>