<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAnalyticUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_analytic_users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('admin_name');
            $table->string('email')->unique();
            $table->string('telephone')->unique();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('government_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_analytic_users');
    }
}
