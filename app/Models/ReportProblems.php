<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportProblems extends Model
{
    use HasFactory;

    protected $table = "report_problems";

    protected $fillable = ["id_manager", "id_org", "email", "role", "problem", "type"];
}
