<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Organizations;
use App\Models\User;
use App\Models\Regions;
use App\Models\Districts;
use App\Models\Government;
use App\Models\TableAnalytics;
use App\Models\TypeOrg;
use App\Models\TypeLocation;
use App\Models\TypeRegions;
use App\Models\TypeProject;
use Carbon\Carbon;

class OrganizationsController extends Controller
{
    private $request;

    public function update(Request $request)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $request->validate([
            "name" => 'required|string|max:255',
            "bin" => 'required|numeric|min:12',
            "region" => 'required|numeric',
            "district" => 'required|numeric',
            "type_org" => 'required|numeric',
            "type_location" => 'required|numeric',
            "type_region" => 'required|numeric',
            "project_capacity" => 'required|numeric',
            "telephone" => 'required|numeric',
            "admin_name" => 'required|string|max:255',
            "location" => 'required|string|max:255',
            "address" => 'required|string|max:255'
        ]);
        
        $this->request = $request;
        $time = Carbon::now();
        
        $organization = Organizations::findOrFail($orgId);
        $organization->name = $request->name;
        $organization->bin = $request->bin;
        $organization->region = $request->region;
        $organization->district = $request->district;
        $organization->type_org = $request->type_org;
        $organization->type_location = $request->type_location;
        $organization->type_region = $request->type_region;
        $organization->project_capacity = $request->project_capacity;
        $organization->telephone = $request->telephone;
        $organization->admin_name = $request->admin_name;
        $organization->location = $request->location;
        $organization->address = $request->address;
        $organization->updated_at = $time;
        $organization->save();

        $tableAnalytics = TableAnalytics::where('id_org', $orgId)->get()->map(function($org) {
            $org->region     = $this->request->region;
            $org->government = Government::where('region_id', $this->request->region)->pluck('government')->first();
            $org->department = Department::where('id', $this->request->district)->pluck('department')->first();
            $org->type_of_department = TypeOrg::where('id', $this->request->type_org)->pluck('type_org')->first();
            $org->save();
            
            return $org;
        });

        $userInfo = $organization;
        $userInfo->type_project  = TypeProject::where('id', $organization->type_project)->pluck('project')->first();
        $userInfo->region        = Regions::where('id', $organization->region)->pluck('region')->first();
        $userInfo->district      = Districts::where('id', $organization->district)->pluck('district')->first();
        $userInfo->type_org      = TypeOrg::where('id', $organization->type_org)->pluck('type_org')->first();
        $userInfo->type_location = TypeLocation::where('id', $organization->type_location)->pluck('type_location')->first();
        $userInfo->type_region   = TypeRegions::where('id', $organization->type_region)->pluck('type_region')->first();

        return response()->json(['user' => $userInfo, 'status' => 'success'], 200);
    }
}
