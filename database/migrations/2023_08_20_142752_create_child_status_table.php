<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_status', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_child');
            $table->unsignedBigInteger('id_teacher');
            $table->unsignedBigInteger('id_group');
            $table->string('status_text');
            $table->integer('status_send');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_status');
    }
}
